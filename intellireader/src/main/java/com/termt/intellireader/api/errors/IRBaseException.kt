/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.api.errors

open class IRBaseException : RuntimeException {
    constructor(): super()
    constructor(message: String, ex: Exception?): super(message, ex)
    constructor(message: String? = null): super(message)
    constructor(ex: Exception): super(ex)
}
