/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.api.errors

open class TransportException : IRBaseException {
    constructor(message: String): super(message)
    constructor(ex: Exception): super(ex)
}

class IRMessageFormatException(message: String) : IRBaseException(message)
class TransportTimeoutException : TransportException {
    constructor(message: String): super(message)
    constructor(ex: Exception): super(ex)
}

class IRMessageIDMismatchException(expected: Int, actual: Int) :
    IRBaseException("expected MSGID=$expected, but receive $actual")
