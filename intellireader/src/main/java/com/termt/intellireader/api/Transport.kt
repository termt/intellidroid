/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.api

import android.content.Context
import android.hardware.usb.UsbDevice
import com.termt.intellireader.entities.TCPParams
import com.termt.intellireader.transport.IRTransport
import com.termt.intellireader.transport.IRTransportSerial
import com.termt.intellireader.transport.IRTransportTCP
import com.termt.intellireader.transport.IRTransportUSB
import serial.Timeout

object Transport {

    private val tcpTransportMap = HashMap<String, IRTransport>()

    @JvmStatic
    fun tcp(params: TCPParams): IRTransport {
        val key = "${params.host}:${params.port}"
        var transport = tcpTransportMap[key]

        if (transport == null) {
            transport = IRTransportTCP(params)
            tcpTransportMap[key] = transport
        }

        return transport
    }

    @JvmStatic
    fun usb(usbDevice: UsbDevice, context: Context): IRTransport = IRTransportUSB(usbDevice, context)


    @JvmStatic
    fun serial(address: String,  baud: Int, timeout: Int): IRTransport = IRTransportSerial(address, baud, timeout)
}
