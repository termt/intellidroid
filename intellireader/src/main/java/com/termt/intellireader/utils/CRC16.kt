/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.utils

internal object CRC16 {

    fun calculate(data: ByteArray): Short {
        var crc = 0x0000
        val polynomial = 0x1021

        data.forEach { byte ->
            for (i in 0..7) {
                val bit = (byte.toInt() shr (7 - i)) and 1 == 1
                val c15 = (crc shr 15) and 1 == 1

                crc = crc shl 1

                if (c15 xor bit) {
                    crc = crc xor polynomial
                }
            }
        }

        return crc.toShort()
    }
}
