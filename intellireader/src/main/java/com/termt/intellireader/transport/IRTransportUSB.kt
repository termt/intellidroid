/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.transport

import android.content.Context
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import com.felhr.usbserial.UsbSerialDevice
import com.felhr.usbserial.UsbSerialInterface
import com.termt.intellireader.api.errors.TransportException
import com.termt.intellireader.api.errors.TransportTimeoutException
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.TimeUnit

class IRTransportUSB(private val usbDevice: UsbDevice, context: Context, private val baudrate : Int = 115200) : IRTransport {

    private val usbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
    private var buffer = ArrayBlockingQueue<Byte>(4096)
    private lateinit var serialPort: UsbSerialDevice

    @Synchronized
    override fun read(count: Int): ByteArray {
        if (!serialPort.isOpen) {
            throw TransportException("usb device not open")
        }

        val res = ByteArray(count)

        for (i in 0 until count) {
            val byte = buffer.poll(5, TimeUnit.SECONDS)

            if (byte == null) {
                throw TransportTimeoutException("failed to read data via usb")
            } else {
                res[i] = byte
            }
        }

        return res
    }

    override fun send(data: ByteArray) {
        if (!serialPort.isOpen) {
            throw TransportException("usb device not open")
        }

        serialPort.write(data)
    }

    override val device: String
        get() = usbDevice.deviceName

    override fun open() {
        val usbConnection = usbManager.openDevice(usbDevice)

        serialPort = UsbSerialDevice.createUsbSerialDevice(usbDevice, usbConnection) ?: throw TransportException("usb device not supported")

        if (serialPort.isOpen) {
            return
        }

        if (!serialPort.open()) {
            throw TransportException("failed to open usb device")
        }

        serialPort.setBaudRate(baudrate)
        serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8)
        serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1)
        serialPort.setParity(UsbSerialInterface.PARITY_NONE)
        serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF)

        serialPort.read { data -> data.forEach { buffer.put(it) } }
    }

    override fun close() {
        serialPort.close()
    }
}
