/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.transport

import android.content.Context
import com.termt.intellireader.api.errors.TransportException
import com.termt.intellireader.api.errors.TransportTimeoutException
import serial.*

class IRTransportSerial(private val address: String, private  val baud: Int,  private val timeout : Int) : IRTransport {

    private lateinit var serialPort: Serial

    @Synchronized
    override fun read(count: Int): ByteArray {

        if (!serialPort.isOpen) {
            throw TransportException("serial device not open")
        }

        val res = ByteArray(count)


        val byte = serialPort.read(res, 0, count)
        if (byte == 0) {
            throw TransportTimeoutException("Serial transport read timeout")
        }

        return res
    }


    override fun send(data: ByteArray) {
        if (!serialPort.isOpen) {
            throw TransportException("serial device not open")
        }

        serialPort.write(data, data.size)
    }

    override val device: String
        get() = address


    override fun open() {
        serialPort = Serial.Builder().setPort(address).setBaudrate(baud).setTimeout(
            Timeout.simpleTimeout(timeout)).create();

        if (serialPort.isOpen) {
            return
        }

        serialPort.open()
        serialPort.read()
    }

    override fun close() {
        serialPort.close()
    }
}
