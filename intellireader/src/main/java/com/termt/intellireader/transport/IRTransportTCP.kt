/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.transport

import com.termt.intellireader.api.errors.TransportException
import com.termt.intellireader.api.errors.TransportTimeoutException
import com.termt.intellireader.entities.TCPParams
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

internal class IRTransportTCP(private val tcpParams: TCPParams) : IRTransport {

    private var socket: Socket = Socket().also { it.close() }

    override fun open() {
        socket.close()
        openIfRequired()
    }

    override fun close() {
        socket.close()
    }

    override fun send(data: ByteArray) {
        try {
            openIfRequired()
            socket.getOutputStream().write(data)
        } catch (ex: IOException) {
            throw toTransportException(ex)
        }
    }

    override val device: String
        get() = tcpParams.host + ":" + tcpParams.port

    override fun read(count: Int): ByteArray {
        try {
            openIfRequired()

            val buffer = (0 until count).map { socket.getInputStream().read() }
                .takeWhile { it != -1 }
                .map { it.toByte() }
                .toByteArray()

            return if (buffer.size != count) {
                throw TransportException("failed to read $count bytes (read only ${buffer.size}))")
            } else {
                buffer
            }
        } catch (ex: IOException) {
            throw toTransportException(ex)
        }
    }

    private fun toTransportException(ex: IOException): TransportException {
        return when (ex) {
            is SocketTimeoutException -> TransportTimeoutException(ex)
            else -> {
                if (ex is SocketException && ex.message?.contains("Socket closed") == true) {
                    TransportTimeoutException(ex)
                } else {
                    socket.close()
                    TransportException(ex)
                }
            }
        }
    }

    private fun openIfRequired() {
        if (socket.isClosed || !socket.isConnected) {
            socket = Socket()
            socket.soTimeout = TimeUnit.SECONDS.toMillis(6).toInt()
            socket.connect(InetSocketAddress(tcpParams.host, tcpParams.port), 8000)
        }
    }
}
