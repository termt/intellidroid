/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.entities

import common.failure.Failure

sealed class TransferResult<out R> {
    class Error(val failure: Failure.FailureResponse): TransferResult<Nothing>()
    class Success<out R>(val result: R): TransferResult<R>()

    fun <T> map(f: (R) -> T): TransferResult<T> =
        when (this) {
            is Error -> Error(this.failure)
            is Success -> Success(f.invoke(this.result))
        }

    fun <T> flatMap(f: (R) -> TransferResult<T>): TransferResult<T> =
        when (this) {
            is Error -> Error(this.failure)
            is Success -> f.invoke(this.result)
        }

    fun unwrap(): R =
        when (this) {
            is Error -> throw IllegalStateException("failed to get value: ${this.failure.error}")
            is Success -> this.result
        }
}
