/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.entities

class IRCommand(val type: Type,
                val module: Module,
                val payload: ByteArray) {

    enum class Type {
        Command,
        Successful,
        Failure,
        Pending,
        Notification,
        Control
    }

    enum class Module {
        Miscellaneous,
        Service,
        ContactL1,
        ContactlessL1,
        ContactlessL2,
        Mifare,
        PinPad,
        StmCard,
        Unspecified,
        Gui,
        TouchScreen,
        Turnstile,
        ComplexPoll,
        Qrcode
    }
}
