/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.extensions

private const val HEX_SYMBOLS = "0123456789ABCDEF"

fun String.isHex(): Boolean = find { !HEX_SYMBOLS.contains(it, true) } == null
fun String.hexAsByteArray(): ByteArray =
    if (isHex()) {
        val value = if (length % 2 == 0) this else "0$this"
        value.chunked(2).map { it.toUpperCase().toInt(16).toByte() }.toByteArray()
    } else {
        throw IllegalArgumentException("invalid hex string: '$this'")
    }

fun ByteArray.toHexString(): String = joinToString("") { "%02x".format(it).toUpperCase() }
