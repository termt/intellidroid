/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.transfer

import com.termt.intellireader.BuildConfig
import com.termt.intellireader.api.errors.IRMessageFormatException
import com.termt.intellireader.api.errors.IRMessageIDMismatchException
import com.termt.intellireader.api.errors.TransportException
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.extensions.toByteArrayBE
import com.termt.intellireader.extensions.toHexString
import com.termt.intellireader.transport.IRTransport
import com.termt.intellireader.utils.CRC16

private var transports_map: HashMap<String, IRTransferConenction> = HashMap()

fun IRTransfer(transport: IRTransport) : IRTransferConenction {
    println("Asked transfer for " + transport.device)

    if (transport.device in transports_map) {
        println("Tranfer present for " + transport.device)
        return transports_map[transport.device]!!
    }


    transports_map.set(transport.device, IRTransferConenction(transport))
    println("Tranfer absent, register new " + transport.device)
    return transports_map[transport.device]!!;
}

class IRTransferConenction(private var transport: IRTransport) {

    private var messageID = 0
    private var opened = false

    fun getMsgId(): Int {
        return messageID
    }

    fun stop() {
        transport.close()
        opened = false
        if (BuildConfig.DEBUG) println("IRTransfer transport stop. Opened false")
    }

    fun reset() {
        stop()
        messageID = 0
        if (BuildConfig.DEBUG) println("IRTransfer set messageID = 0")
    }

    fun read(): IRCommand {
        openIfRequired()

        while (true) {
            val response = readMessage()
            if (BuildConfig.DEBUG)
                println("read(${response.messageID}): " +
                        "type=${response.command.type}, " +
                        "module=${response.command.module}")

            if (response.command.hasValidMessageID() && response.messageID != messageID.toByte()) {
                throw IRMessageIDMismatchException(messageID, response.messageID.toInt())
            }

            return response.command
        }
    }

    fun send(request: IRCommand) {
        openIfRequired()

        if (!request.cancel()) {
            incrementMessageID()
        }

        if (BuildConfig.DEBUG)
            println("send($messageID): " +
                    "type=${request.type}, " +
                    "module=${request.module}")
        sendMessage(IRMessage(messageID.toByte(), request))
    }

    private fun sendMessage(message: IRMessage) {
        val body = byteArrayOf(
            message.messageID,
            message.command.module.toByte(),
            message.command.type.toByte()
        ) + message.command.payload
        val bodyLength = body.size.toShort().toByteArrayBE()

        val rawMessage = IR_HEADER + bodyLength + body
        val checksum = CRC16.calculate(rawMessage)

        return transport.send(rawMessage + checksum.toByteArrayBE())
    }

    private fun readMessage(): IRMessage {
        val header = transport.read(2)

        if (!header.contentEquals(IR_HEADER)) {

            throw IRMessageFormatException("invalid ir message header: " + header.toHexString() + "; expected: " + IR_HEADER.toHexString() )
        }

        val rawBodyLength = transport.read(2)
        val bodyLength = rawBodyLength.toHexString().toInt(16)
        val rawBody = transport.read(bodyLength)

        val checksum = transport.read(2).toHexString().toInt(16).toShort()

        val message = header + rawBodyLength + rawBody

        return if (CRC16.calculate(message) == checksum) {
            rawBody.toIRMessage()
        } else {
            throw IRMessageFormatException("invalid crc for message: ${message.toHexString()}. Got ${CRC16.calculate(message)}; expected $checksum")
        }
    }

    private fun incrementMessageID(){
        ++messageID

        if (messageID > 255) {
            messageID = 1
        }
    }

    private fun openIfRequired() {
        if (!opened) {
            try {
                transport.open()
                opened = true
            } catch (ex: TransportException) {
                opened = false
                throw ex
            }
        }
    }

    companion object {

        private lateinit var transports_map: HashMap<String, IRTransport>

        private val IR_HEADER = "IR".toByteArray(Charsets.US_ASCII)
        private val COMMAND_TYPE_WITH_VALID_MESSAGE_ID = listOf(
            IRCommand.Type.Successful,
            IRCommand.Type.Failure,
            IRCommand.Type.Pending
        )

        private fun ByteArray.toIRMessage(): IRMessage {
            if (size < 3) {
                throw IRMessageFormatException("ir body in incorrect")
            }

            val messageID = this[0]
            val module = this[1].toModule()
            val messageType = this[2].toCommandType()

            val command = IRCommand(messageType, module, takeLast(size - 3).toByteArray())
            return IRMessage(messageID, command)
        }

        private fun Byte.toModule(): IRCommand.Module =
            when (this.toInt()) {
                0x00 -> IRCommand.Module.Unspecified
                0x01 -> IRCommand.Module.Miscellaneous
                0x02 -> IRCommand.Module.ContactL1
                0x03 -> IRCommand.Module.ContactlessL1
                0x04 -> IRCommand.Module.ContactlessL2
                0x05 -> IRCommand.Module.Mifare
                0x06 -> IRCommand.Module.PinPad
                0x07 -> IRCommand.Module.Service
                0x08 -> IRCommand.Module.StmCard
                0x0A -> IRCommand.Module.Gui
                0x0B -> IRCommand.Module.TouchScreen
                0x0C -> IRCommand.Module.ComplexPoll
                0x0D -> IRCommand.Module.Qrcode
                else -> throw IRMessageFormatException("unknown module '$this'")
            }

        private fun IRCommand.Module.toByte(): Byte =
            when (this) {
                IRCommand.Module.Unspecified -> 0x00
                IRCommand.Module.Miscellaneous -> 0x01
                IRCommand.Module.ContactL1 -> 0x02
                IRCommand.Module.ContactlessL1 -> 0x03
                IRCommand.Module.ContactlessL2 -> 0x04
                IRCommand.Module.Mifare -> 0x05
                IRCommand.Module.PinPad -> 0x06
                IRCommand.Module.Service -> 0x07
                IRCommand.Module.StmCard -> 0x08
                IRCommand.Module.Gui -> 0x0A
                IRCommand.Module.TouchScreen ->0x0B
                IRCommand.Module.ComplexPoll -> 0x0C
                IRCommand.Module.Qrcode -> 0x0D
                IRCommand.Module.Turnstile -> 0x10
            }

        private fun Byte.toCommandType(): IRCommand.Type =
            when (this.toInt()) {
                0x01 -> IRCommand.Type.Command
                0x02 -> IRCommand.Type.Successful
                0x03 -> IRCommand.Type.Failure
                0x04 -> IRCommand.Type.Pending
                0x05 -> IRCommand.Type.Notification
                0x06 -> IRCommand.Type.Control
                else -> throw IRMessageFormatException("unknown command type '$this'")
            }

        private fun IRCommand.Type.toByte(): Byte =
            when (this) {
                IRCommand.Type.Command -> 0x01
                IRCommand.Type.Successful -> 0x02
                IRCommand.Type.Failure -> 0x03
                IRCommand.Type.Pending -> 0x04
                IRCommand.Type.Notification -> 0x05
                IRCommand.Type.Control -> 0x06
            }

        private fun IRCommand.hasValidMessageID(): Boolean = type in COMMAND_TYPE_WITH_VALID_MESSAGE_ID && module != IRCommand.Module.Unspecified

        private fun IRCommand.cancel(): Boolean =
            type == IRCommand.Type.Control && module == IRCommand.Module.Unspecified
    }

    private class IRMessage(val messageID: Byte, val command: IRCommand)
}
