/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.transfer

import com.termt.intellireader.BuildConfig
import com.termt.intellireader.api.errors.TransportException
import com.termt.intellireader.api.errors.TransportTimeoutException
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import common.failure.Failure
import java.io.IOException

class IRExchange(private val transfer: IRTransferConenction) {

    fun sequence(request: IRCommand): Iterator<IRCommand> {
        transfer.send(request)
        return IRMessagesIterator(transfer)
    }

    fun reset() {
        //Reading answers from IReader, till we got TimeoutException
        if (BuildConfig.DEBUG) println("IRExchange reset. go thru iterator")
        while (true) {
            try {
                if (BuildConfig.DEBUG) println("IRExchange reset. next")
                IRMessagesIterator(transfer).next()
            } catch (e: Exception) {
                e.printStackTrace()
                when (e) {
                    is TransportTimeoutException -> {
                        break
                    }
                }
            }
        }
        transfer.reset()
    }

    fun RawTransaction(request: IRCommand) = transfer.send(request);

    fun withResult(request: IRCommand): IRCommand = sequence(request).asSequence().last()

    fun <T> withTypedResult(request: IRCommand, serializer: (data: ByteArray) -> T): TransferResult<T> =
            try {
                synchronized(transfer) {
                    val response = withResult(request)

                    when (response.type) {
                        IRCommand.Type.Failure -> TransferResult.Error(Failure.FailureResponse.parseFrom(response.payload))
                        IRCommand.Type.Successful -> TransferResult.Success(serializer.invoke(response.payload))
                        else -> throw IllegalStateException("unsupported response type (${response.type.name})")
                    }
                }
            } catch (ex: Exception) {
                when (ex) {
                    is TransportException, is IOException -> TransferResult.Error(
                        Failure.FailureResponse.newBuilder()
                            .setError(Failure.Error.GENERAL_FAIL)
                            .setErrorString(ex.message ?: ex.javaClass.name)
                            .build()
                    )
                    else -> throw ex
                }
            }

    private class IRMessagesIterator(private val transfer: IRTransferConenction) : Iterator<IRCommand> {

        var hasNext = true
        var pendingReceived = false

        override fun hasNext(): Boolean = hasNext

        override fun next(): IRCommand {
            while (true) {
                try {
                    val response = transfer.read()

                    when (response.type) {
                        IRCommand.Type.Successful, IRCommand.Type.Failure -> { hasNext = false }
                        IRCommand.Type.Pending -> { pendingReceived = true }
                    }
                    if (BuildConfig.DEBUG)
                        println("IRMessagesIterator: send(${transfer.getMsgId()}); Next($hasNext); Pending($pendingReceived)")
                    return response
                } catch (ex: TransportException) {
                    when (ex) {
                        is TransportTimeoutException -> {
                            if (!pendingReceived) {
                                throw ex
                            }
                        }
                        else -> {
                            hasNext = false
                            throw ex
                        }
                    }
                }
            }
        }
    }
}
