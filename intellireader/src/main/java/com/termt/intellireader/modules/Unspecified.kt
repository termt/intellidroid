/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import com.termt.intellireader.BuildConfig
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.transfer.IRTransferConenction

class Unspecified(transfer: IRTransferConenction): Module(transfer) {

    fun cancel() {
        if (BuildConfig.DEBUG) println("IRCommand.Module.Unspecified -> cancel")
        val result = irExchange.RawTransaction(create(ByteArray(0)))
    }

    fun reset(): Boolean {
        if (BuildConfig.DEBUG) println("IRCommand.Module.Unspecified -> reset")
        cancel()
        irExchange.reset()
        return true
    }

    override fun create(payload: ByteArray): IRCommand =
        IRCommand(IRCommand.Type.Control, module, payload)

    override val module: IRCommand.Module = IRCommand.Module.Unspecified
}
