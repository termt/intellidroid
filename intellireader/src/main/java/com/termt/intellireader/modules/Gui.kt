/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.google.protobuf.ByteString
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import gui.input_dialog.InputDialogOuterClass
import gui.menu_dialog.MenuDialogOuterClass
import gui.picture_id.PictureIdOuterClass

import gui.screen.Screen
import gui.widget.WidgetOuterClass


class Gui(transfer: IRTransferConenction): Module(transfer) {

    fun showScreen(id: PictureIdOuterClass.PictureId): TransferResult<Unit> =
            irExchange.withTypedResult(
                    create(
                            Commands.Gui.newBuilder().setShowScreen(Screen.ShowScreen.newBuilder()
                                    .setRoot(WidgetOuterClass.Widget.newBuilder()
                                            .setPicture(WidgetOuterClass.Picture.newBuilder()
                                                    .setPictureId(id))))
                                    .build()
                                    .toByteArray()
                    )
            ) {}


    fun clearScreen(): TransferResult<Unit> =
            irExchange.withTypedResult(
                    create(
                            Commands.Gui.newBuilder().setShowScreen(Screen.ShowScreen.newBuilder().clearRoot())
                                    .build()
                                    .toByteArray()
                    )
            ) {}


    fun showText(text: String): TransferResult<Unit> =
            irExchange.withTypedResult(
                    create(
                            Commands.Gui.newBuilder().setShowScreen(Screen.ShowScreen.newBuilder().setRoot(WidgetOuterClass.Widget.newBuilder()
                                    .setText(WidgetOuterClass.Text.newBuilder().setText(text).build())))
                                    .build()
                                    .toByteArray()
                    )
            ) {}

    fun showText(text: String, pic: PictureIdOuterClass.PictureId): TransferResult<Unit>
    {
        val picwidget = WidgetOuterClass.Widget.newBuilder().setPicture(WidgetOuterClass.Picture.newBuilder().setPictureId(pic))
        val textwidget = WidgetOuterClass.Widget.newBuilder().setText(WidgetOuterClass.Text.newBuilder().setText(text))
        val vertlay = WidgetOuterClass.Widget.newBuilder().setVerticalLayout(WidgetOuterClass.VerticalLayout.newBuilder().addWidgets(picwidget).addWidgets(textwidget))
        val root = Screen.ShowScreen.newBuilder().setRoot(vertlay)
        val cmd = Commands.Gui.newBuilder().setShowScreen(root)

        return irExchange.withTypedResult <Unit> (create(cmd.build().toByteArray())) {}
    }


    fun showTextAndQRScreen(text: String, qr :String): TransferResult<Unit>
        {
                val qrwidget = WidgetOuterClass.Widget.newBuilder().setQrCode(WidgetOuterClass.QrCode.newBuilder().setText(ByteString.copyFromUtf8(qr)))
                val textwidget = WidgetOuterClass.Widget.newBuilder().setText(WidgetOuterClass.Text.newBuilder().setText(text))
                val vertlay = WidgetOuterClass.Widget.newBuilder().setVerticalLayout(WidgetOuterClass.VerticalLayout.newBuilder().addWidgets( qrwidget).addWidgets(textwidget))
                val root = Screen.ShowScreen.newBuilder().setRoot(vertlay)
                val cmd = Commands.Gui.newBuilder().setShowScreen(root)

                return irExchange.withTypedResult <Unit> (create(cmd.build().toByteArray())) {}
        }

        fun showMenu(caption: String, items: MenuDialogOuterClass.ItemList, timeout: Int): TransferResult<MenuDialogOuterClass.SelectedItem> =
            irExchange.withTypedResult(
                create(
                    Commands.Gui.newBuilder().
                    setMenuDialog(
                        MenuDialogOuterClass.MenuDialog.newBuilder()
                            .setCaption(caption)
                            .setTimeout(timeout)
                            .setItemList(items)

                    )
                        .build()
                        .toByteArray()
                ),
                MenuDialogOuterClass.SelectedItem::parseFrom
            );

    fun getUserInput(caption: String, placeholder: String, timeout: Int): TransferResult<InputDialogOuterClass.EnteredText> =
        irExchange.withTypedResult(
            create(
                Commands.Gui.newBuilder().
                setInputDialog(
                    InputDialogOuterClass.InputDialog.newBuilder()
                        .setCaption(caption)
                        .setPlaceholder(placeholder)
                        .setTimeout(timeout)
                )
                    .build()
                    .toByteArray()
            ),
            InputDialogOuterClass.EnteredText::parseFrom
        );

    fun getUserInput(caption: String, placeholder: String, timeout: Int, layouts: List<InputDialogOuterClass.KeypadLayout>): TransferResult<InputDialogOuterClass.EnteredText> {
        val input_dialog_builder = InputDialogOuterClass.InputDialog.newBuilder()
            .setCaption(caption)
            .setPlaceholder(placeholder)
            .setTimeout(timeout)

        if (layouts != null && layouts.isNotEmpty())
            input_dialog_builder
                .clearLayouts()
                .addAllLayouts(layouts)

        return irExchange.withTypedResult(
            create(
                Commands.Gui.newBuilder().
                setInputDialog(input_dialog_builder)
                    .build()
                    .toByteArray()
            ),
            InputDialogOuterClass.EnteredText::parseFrom
        )
    }


    override val module: IRCommand.Module = IRCommand.Module.Gui
}
