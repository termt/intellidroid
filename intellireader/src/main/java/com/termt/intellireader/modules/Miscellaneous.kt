/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.google.protobuf.ByteString
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import contactless.transceive.Transceive
import misc.buzzer.Buzzer
import misc.device.Device
import misc.leds.LedsOuterClass
import misc.reboot.RebootOuterClass
import misc.baudrate.BaudrateOuterClass
import misc.echo.EchoOuterClass

class Miscellaneous(transfer: IRTransferConenction): Module(transfer) {

    fun setLeds(request: LedsOuterClass.Leds): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.Miscellaneous.newBuilder()
                    .setSetLedsState(request)
                    .build()
                    .toByteArray()
            )
        ) {}


    fun setEcho(request: ByteString, pending: Boolean): TransferResult<EchoOuterClass.Echo> =

        irExchange.withTypedResult(
            create(
                Commands.Miscellaneous.newBuilder()
                    .setGetEcho(EchoOuterClass.GetEcho.newBuilder()
                        .setSendPending(pending)
                        .setData(request)
                        .build())
                    .build()
                    .toByteArray()
            ),
            EchoOuterClass.Echo::parseFrom
        )

    fun makeSound(notes : Iterable<Buzzer.Note>): TransferResult <Unit>  =
        irExchange.withTypedResult(
            create(Commands.Miscellaneous.newBuilder()
                .setMakeSound(Buzzer.MakeSound.newBuilder().addAllMelody(notes))
                .build()
                .toByteArray())
        ) {}


    fun readDeviceInfo(): TransferResult<Device.DeviceInfo> =
        irExchange.withTypedResult(
            create(
                Commands.Miscellaneous.newBuilder()
                    .setReadDeviceInfo(Device.ReadDeviceInfo.getDefaultInstance())
                    .build()
                    .toByteArray()
            ),
            Device.DeviceInfo::parseFrom
        )

    fun rebootDevice(request: RebootOuterClass.Reboot): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.Miscellaneous.newBuilder()
                    .setRebootDevice(request)
                    .build()
                    .toByteArray()
            )
        ) {}

    fun switchBaudrate(request: BaudrateOuterClass.Baudrate): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.Miscellaneous.newBuilder()
                    .setChangeBaudrate(BaudrateOuterClass.ChangeBaudrate.newBuilder().setBaudrate(request))
                    .build()
                    .toByteArray()
            )
        ) {}


    override val module: IRCommand.Module = IRCommand.Module.Miscellaneous
}
