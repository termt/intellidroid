/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import srv.firmware_update.FirmwareUpdate

class Service(transfer: IRTransferConenction): Module(transfer) {

    fun prepareUpdate(request: FirmwareUpdate.Prepare): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.Service.newBuilder()
                    .setPrepareUpdate(request)
                    .build()
                    .toByteArray()
            )
        ) {}

    fun updateBlock(request: FirmwareUpdate.UpdateBlock): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.Service.newBuilder()
                    .setUpdateBlock(request)
                    .build()
                    .toByteArray()
            )
        ) {}

    fun applyUpdate(request: FirmwareUpdate.Apply): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.Service.newBuilder()
                    .setApplyUpdate(request)
                    .build()
                    .toByteArray()
            )
        ) {}

    fun rollbackUpdate(request: FirmwareUpdate.Rollback): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.Service.newBuilder()
                    .setRollbackUpdate(request)
                    .build()
                    .toByteArray()
            )
        ) {}


    override val module: IRCommand.Module = IRCommand.Module.Service
}
