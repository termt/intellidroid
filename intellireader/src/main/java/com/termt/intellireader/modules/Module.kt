/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.transfer.IRExchange
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction

abstract class Module(transfer: IRTransferConenction) {

    val irExchange = IRExchange(transfer)

    abstract val module: IRCommand.Module
    open fun create(payload: ByteArray): IRCommand =
        IRCommand(IRCommand.Type.Command, module, payload)
}
