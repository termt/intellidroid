/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import contactless.emv_removal.EmvRemovalOuterClass
import contactless.iso14443_4.Iso144434
import contactless.poll.PollForTokenOuterClass
import contactless.poll.PollForTokenOuterClass.PollForToken
import contactless.rf_field.RfField
import contactless.token.TokenOuterClass
import contactless.transceive.Transceive

class ContactlessL1(transfer: IRTransferConenction): Module(transfer) {

    @Deprecated(message = "use pollForTokenLow for screen devices",
                replaceWith = ReplaceWith(
                    expression = "pollForTokenLow(request)",
                    imports = ["com.termt.intellireader.modules.ContactlessL1"]
                ))
    fun pollForToken(request: PollForTokenOuterClass.PollForToken): TransferResult<TokenOuterClass.Token> =
        irExchange.withTypedResult(
            create(
                Commands.ContactlessLevel1.newBuilder()
                    .setPollForToken(request)
                    .build()
                    .toByteArray()
            ),
            TokenOuterClass.Token::parseFrom
        )

    fun pollForTokenLow(request: PollForTokenOuterClass.PollForToken): TransferResult<TokenOuterClass.Token> =
        irExchange.withTypedResult(
            create(
                Commands.ContactlessLevel1.newBuilder()
                    .setPollForToken(PollForToken.newBuilder(request)
                        .setPollingMode(PollForTokenOuterClass.PollingMode.LOW_POWER_POLLING)
                        .build())
                    .build()
                    .toByteArray()
            ),
            TokenOuterClass.Token::parseFrom
        )

    fun fieldPowerOff(): TransferResult<Unit> =
        irExchange.withTypedResult(create(RfField.PowerOffField.newBuilder().build().toByteArray())) {}

    fun emvRemoval(request: EmvRemovalOuterClass.EmvRemoval): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.ContactlessLevel1.newBuilder()
                    .setEmvRemoval(request)
                    .build()
                    .toByteArray()
            )
        ) {}

    fun transcieveBitArray(request: Transceive.TransceiveBitArray): TransferResult<Transceive.BitArray> =
        irExchange.withTypedResult(
            create(
                Commands.ContactlessLevel1.newBuilder()
                    .setTsvBitArray(request)
                    .build()
                    .toByteArray()
            ),
            Transceive.BitArray::parseFrom
        )

    fun transceiveISO14443_4Command(request: Iso144434.Command): TransferResult<Iso144434.Response> =
        irExchange.withTypedResult(
            create(Commands.ContactlessLevel1.newBuilder()
                .setIso144434Command(request)
                .build().
                toByteArray()
            ),
            Iso144434.Response::parseFrom
        )

    override val module: IRCommand.Module = IRCommand.Module.ContactlessL1
}
