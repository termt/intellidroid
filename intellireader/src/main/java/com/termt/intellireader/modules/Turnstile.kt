package com.termt.intellireader.modules

import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import contactless.poll.PollForTokenOuterClass
import contactless.token.TokenOuterClass

class Turnstile(transfer: IRTransferConenction): Module(transfer) {

    fun allowOnePass(request: turnstile.passage.Passage.AllowOnePass): TransferResult<TokenOuterClass.Token> =
        irExchange.withTypedResult(
            create(
                Commands.Turnstile.newBuilder()
                    .setAllowOnePass(request)
                    .build()
                    .toByteArray()
            ),
            TokenOuterClass.Token::parseFrom
        )

    override val module: IRCommand.Module = IRCommand.Module.Turnstile
}