/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransferConenction
import contactless.poll.PollForTokenOuterClass
import contactless.poll.PollForTokenOuterClass.PollForToken
import contactless.transaction.Transaction
import contactless.transaction.Transaction.PerformTransaction

class ContactlessL2(transfer: IRTransferConenction): Module(transfer) {

    @Deprecated(message = "use performEMVTransactionLow for screen devices",
        replaceWith = ReplaceWith(
            expression = "performEMVTransactionLow(request)",
            imports = ["com.termt.intellireader.modules.ContactlessL2"]
        ))
    fun performEMVTransaction(request: PerformTransaction): TransferResult<Transaction.TransactionResult> =
        irExchange.withTypedResult(
            create(
                Commands.ContactlessLevel2.newBuilder()
                    .setPerformTransaction(request)
                    .build()
                    .toByteArray()
            ),
            Transaction.TransactionResult::parseFrom
        )

    fun performEMVTransactionLow(request: Transaction.PerformTransaction): TransferResult<Transaction.TransactionResult> =
        irExchange.withTypedResult(
            create(
                Commands.ContactlessLevel2.newBuilder()
                    .setPerformTransaction(PerformTransaction.newBuilder(request)
                        .setPollForToken(PollForToken.newBuilder(request.pollForToken)
                            .setPollingMode(PollForTokenOuterClass.PollingMode.LOW_POWER_POLLING)
                            .build())
                        .build())
                    .build()
                    .toByteArray()
            ),
            Transaction.TransactionResult::parseFrom
        )

    @Deprecated(message = "use startEMVTransactionLow for screen devices",
        replaceWith = ReplaceWith(
            expression = "startEMVTransactionLow(request)",
            imports = ["com.termt.intellireader.modules.ContactlessL2"]
        ))
    fun startEMVTransaction(request: PerformTransaction): Iterator<IRCommand> =
        irExchange.sequence(
            create(
                Commands.ContactlessLevel2.newBuilder()
                    .setPerformTransaction(request)
                    .build()
                    .toByteArray()
            )
        )

    fun startEMVTransactionLow(request: Transaction.PerformTransaction): Iterator<IRCommand> =
        irExchange.sequence(
            create(
                Commands.ContactlessLevel2.newBuilder()
                    .setPerformTransaction(PerformTransaction.newBuilder(request)
                        .setPollForToken(PollForToken.newBuilder(request.pollForToken)
                            .setPollingMode(PollForTokenOuterClass.PollingMode.LOW_POWER_POLLING)
                            .build())
                        .build())
                    .build()
                    .toByteArray()
            )
        )

    override val module: IRCommand.Module = IRCommand.Module.ContactlessL2
}
