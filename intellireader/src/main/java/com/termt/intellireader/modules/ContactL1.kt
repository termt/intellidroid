/*
 * Copyright (C) 2020 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import contact.power_on.PowerOn
import contact.power_off.PowerOff
import contact.iso7816_4.Iso78164

class ContactL1(transfer: IRTransferConenction) : Module(transfer) {

    fun powerOn(request: PowerOn.PowerOnCard): TransferResult<PowerOn.ContactCard> =
        irExchange.withTypedResult(
            create(
                Commands.ContactLevel1.newBuilder()
                    .setPowerOnCard(request)
                    .build()
                    .toByteArray()
            ),
            PowerOn.ContactCard::parseFrom
        )

    fun powerOff(request: PowerOff.PowerOffCard): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(
                Commands.ContactLevel1.newBuilder()
                    .setPowerOffCard(request)
                    .build()
                    .toByteArray()
            ),
            {}
        )

    fun transmitApdu(request: Iso78164.TransmitApdu): TransferResult<Iso78164.ResponseApdu> =
        irExchange.withTypedResult(
            create(
                Commands.ContactLevel1.newBuilder()
                    .setTransmitApdu(request)
                    .build()
                    .toByteArray()
            ),
            Iso78164.ResponseApdu::parseFrom
        )

    override val module: IRCommand.Module = IRCommand.Module.ContactL1
}
