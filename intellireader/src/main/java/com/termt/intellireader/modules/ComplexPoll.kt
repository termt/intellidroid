/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import complex.poll.PollForEventOuterClass
import complex.poll.PollForEventOuterClass.PollForEvent

class ComplexPoll(transfer: IRTransferConenction): Module(transfer) {
        fun pollForEvent(pollForEvent: PollForEvent): TransferResult<PollForEventOuterClass.Event> =
            irExchange.withTypedResult(
                    create(
                            Commands.Complex.newBuilder()
                                .mergePollForEvent(pollForEvent)
                                .build()
                                .toByteArray()
                    ),
                    PollForEventOuterClass.Event::parseFrom
            )

    override val module: IRCommand.Module = IRCommand.Module.ComplexPoll
}
