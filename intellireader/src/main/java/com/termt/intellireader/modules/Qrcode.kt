/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import qrcode.poll.PollForQrcode

class Qrcode(transfer: IRTransferConenction): Module(transfer) {
        fun pollForQrRaw(timeout: Int): TransferResult<qrcode.event.QrcodeEvent.QrCodeEvent> =
            irExchange.withTypedResult(
                    create(
                            Commands.QrCode.newBuilder().setPollForQrcode(
                                    PollForQrcode.PollForQrCode.newBuilder()
                                            .setEventType(PollForQrcode.EventType.RAW_DATA)
                                            .setTimeout(timeout)
                            )
                                    .build()
                                    .toByteArray()
                    ),
                    qrcode.event.QrcodeEvent.QrCodeEvent::parseFrom
            )

    override val module: IRCommand.Module = IRCommand.Module.Qrcode
}
