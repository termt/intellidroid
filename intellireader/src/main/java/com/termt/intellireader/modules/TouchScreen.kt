/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import touchscreen.event.TouchscreenEventOuterClass
import touchscreen.poll.PollTouchscreenOuterClass


class TouchScreen(transfer: IRTransferConenction): Module(transfer) {


    fun pollForTouchEvent(type: PollTouchscreenOuterClass.EventType, timeout: Int): TransferResult<TouchscreenEventOuterClass.TouchscreenEvent> =
        irExchange.withTypedResult(
            create(
                Commands.Touchscreen.newBuilder().
                setPollTouchscreen(
                    PollTouchscreenOuterClass.PollTouchscreen.newBuilder()
                        .setEventType(type)
                        .setTimeout(timeout)
                )
                .build()
                .toByteArray()
            ),
            TouchscreenEventOuterClass.TouchscreenEvent::parseFrom
        )

    fun poolForTouchCoordinates(timeout: Int): TransferResult<TouchscreenEventOuterClass.TouchscreenEvent> = pollForTouchEvent(PollTouchscreenOuterClass.EventType.COORDINATES, timeout)
    fun poolForTouchWidget(timeout: Int): TransferResult<TouchscreenEventOuterClass.TouchscreenEvent> = pollForTouchEvent(PollTouchscreenOuterClass.EventType.WIDGET_ID, timeout)

    override val module: IRCommand.Module = IRCommand.Module.TouchScreen
}
