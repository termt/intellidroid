/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import Commands
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import mifare.classic.auth.Auth
import mifare.classic.bulk.Bulk
import mifare.classic.counter.commit.Commit
import mifare.classic.counter.copy.Copy
import mifare.classic.counter.get.Get
import mifare.classic.counter.modify.Modify
import mifare.classic.counter.set.Set
import mifare.classic.read.Read
import mifare.classic.write.Write

class Mifare(transfer: IRTransferConenction): Module(transfer) {

    fun authOnClearKey(request: Auth.ClearKey): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicAuthOnClearKey(request)
                .build()
                .toByteArray()
            )
        ) {}

    fun authOnSamKey(request: Auth.SamKey): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicAuthOnSamKey(request)
                .build()
                .toByteArray()
            )
        ) {}

    fun read(request: Read.ReadBlocks): TransferResult<ByteArray> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicReadBlocks(request)
                .build()
                .toByteArray()
            ),
            Read.Blocks::parseFrom
        ).map { it.data.toByteArray() }

    fun write(request: Write.WriteBlocks): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicWriteBlocks(request)
                .build()
                .toByteArray()
            )
        ) {}

    fun bulk(request: Bulk.BulkOperation): TransferResult<Bulk.BulkResult> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicBulkOperation(request)
                .build()
                .toByteArray()
            ),
            Bulk.BulkResult::parseFrom
        )

    fun getCounterValue(request: Get.GetCounter): TransferResult<Int> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicGetCounter(request)
                .build()
                .toByteArray()
            ),
            Get.Counter::parseFrom
        ).map { it.value }

    fun setCounterValue(request: Set.SetCounter): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicSetCounter(request)
                .build()
                .toByteArray()
            )
        ) {}

    fun modifyCounterValue(request: Modify.ModifyCounter): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicModifyCounter(request)
                .build()
                .toByteArray()
            )
        ) {}

    fun copyCounterValue(request: Copy.CopyCounter): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicCopyCounter(request)
                .build()
                .toByteArray()
            )
        ) {}

    fun commitCounterValue(request: Commit.CommitCounter): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.Mifare.newBuilder()
                .setMfrClassicCommitCounter(request)
                .build()
                .toByteArray()
            )
        ) {}

    override val module: IRCommand.Module = IRCommand.Module.Mifare
}
