/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader.modules

import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import stmcard.sri512.read.Read
import stmcard.sri512.write.Write

class StmCard(transfer: IRTransferConenction): Module(transfer) {

    fun read(request: Read.ReadBlocks): TransferResult<Read.Blocks> =
        irExchange.withTypedResult(
            create(Commands.StmCard.newBuilder()
                .setStmSri512ReadBlocks(request)
                .build()
                .toByteArray()
            ),
            Read.Blocks::parseFrom
        )

    fun write(request: Write.WriteBlocks): TransferResult<Unit> =
        irExchange.withTypedResult(
            create(Commands.StmCard.newBuilder()
                .setStmSri512WriteBlocks(request)
                .build()
                .toByteArray()
            )
        ) {}

    override val module: IRCommand.Module = IRCommand.Module.StmCard
}
