syntax = "proto2";

import "mifare/plus/key_type.proto";
import "mifare/plus/security_level.proto";
import "mifare/av2/args.proto";

package mifare.plus.auth;

// Authenticates to Mifare Plus card using clear key
message ClearKey {
    // Specifies which type of key to use for authentication
    required mifare.plus.key_type.KeyType key_type = 1;

    // Value of the Key (16 bytes)
    required bytes clear_key = 2;

    // Specifies which security level to use for authentication
    optional mifare.plus.security_level.SecurityLevel security_level = 3 [default = SL3];

    // AES128 key diversification input data if required
    optional bytes diversification_input = 4;
}

// Authenticates to Mifare Plus card using NXP SAM AV2 module
message SamKey {
    // Specifies which type of key to use for authentication
    required mifare.plus.key_type.KeyType key_type = 1;

    // Specifies SAM AV2 arguments that will be used in authentication
    required mifare.av2.args.AuthenticationArguments av2_args = 2;

    // Specifies which security level to use for authentication
    optional mifare.plus.security_level.SecurityLevel security_level = 3 [default = SL3];

    // AES128 key diversification input data if required
    optional bytes diversification_input = 4;
}

// Authenticates to Mifare Plus case using key from internal secure storage
message InternalKey {
    // Specifies which type of key to use for authentication
    required mifare.plus.key_type.KeyType key_type = 1;

    // Index of the key within internal storage
    required uint32 key_index = 2;

    // Specifies which security level to use for authentication
    optional mifare.plus.security_level.SecurityLevel security_level = 3 [default = SL3];

    // AES128 key diversification input data if required
    optional bytes diversification_input = 4;
}

// No data carried with successful response
