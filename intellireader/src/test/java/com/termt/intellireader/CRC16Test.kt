/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader

import com.termt.intellireader.extensions.hexAsByteArray
import com.termt.intellireader.utils.CRC16
import junit.framework.Assert.assertEquals
import org.junit.Test

class CRC16Test {

    @Test
    fun simpleArrayTest() {
        assertEquals(0x635A, CRC16.calculate("4952000B0103020803120412345678".hexAsByteArray()))
    }
}
