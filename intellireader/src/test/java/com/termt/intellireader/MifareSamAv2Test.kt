/*
 * Copyright (C) 2020 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader

import com.google.protobuf.ByteString
import com.termt.intellireader.api.Transport
import com.termt.intellireader.entities.TCPParams
import com.termt.intellireader.modules.ContactL1
import com.termt.intellireader.modules.ContactlessL1
import com.termt.intellireader.modules.Mifare
import com.termt.intellireader.transfer.IRTransfer
import contact.card_slot.CardSlotOuterClass
import contact.iso7816_4.Iso78164
import contact.power_off.PowerOff
import contact.power_on.PowerOn
import contactless.poll.PollForTokenOuterClass
import mifare.av2.args.Args
import mifare.classic.auth.Auth
import mifare.classic.counter.commit.Commit
import mifare.classic.counter.get.Get
import mifare.classic.counter.set.Set
import mifare.classic.counter.modify.Modify
import mifare.classic.read.Read
import mifare.classic.write.Write
import org.junit.After
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MifareSamAv2Test {

    @Before
    fun setUp() {
        CONTACT.powerOn(PowerOn.PowerOnCard.newBuilder().setSlot(SAM_SLOT).build()).unwrap()
        CONTACTLESS_L1.pollForToken(PollForTokenOuterClass.PollForToken.newBuilder().setTimeout(15_000).build()).unwrap()
        MIFARE.authOnSamKey(AUTH_ON_SAM_KEY).unwrap()
    }

    @After
    fun setDown() {
        CONTACT.powerOff(PowerOff.PowerOffCard.newBuilder().setSlot(SAM_SLOT).build()).unwrap()
    }

    @Test
    fun getVersion() {
        val apdu = ByteString.copyFrom(byteArrayOf(0x80.toByte(), 0x60, 0x00, 0x00, 0x00))

        val rapdu = CONTACT.transmitApdu(
            Iso78164.TransmitApdu
                .newBuilder()
                .setSlot(SAM_SLOT)
                .setCommandApdu(apdu)
                .build()
        ).unwrap()

        assertEquals(rapdu.trailer, 0x9000)
    }

    @Test
    fun writeTest() {
        val data = ByteArray(16) { 0x37 }
        val blockCount = 1

        MIFARE.write(
            Write.WriteBlocks.newBuilder()
                .setStartBlock(BLOCK)
                .setData(ByteString.copyFrom(data))
                .build()
        ).unwrap()

        val readData = MIFARE.read(
            Read.ReadBlocks.newBuilder()
                .setStartBlock(BLOCK)
                .setBlockCount(blockCount)
                .build()
        ).unwrap()

        assertArrayEquals(data, readData)
    }

    @Test
    fun setCounterTest() {
        val value = 134

        MIFARE.setCounterValue(
            Set.SetCounter.newBuilder()
                .setDstBlock(BLOCK)
                .setValue(value)
                .build()
        ).unwrap()

        val readCounterValue = MIFARE.getCounterValue(
            Get.GetCounter.newBuilder()
                .setSrcBlock(BLOCK)
                .build()
        ).unwrap()

        assertEquals(value, readCounterValue)
    }

    @Test
    fun modifyCounterTest() {
        val value = 121
        val operand = -3

        MIFARE.setCounterValue(
            Set.SetCounter.newBuilder()
                .setDstBlock(BLOCK)
                .setValue(value)
                .build()
        ).unwrap()

        MIFARE.modifyCounterValue(
            Modify.ModifyCounter.newBuilder()
                .setSrcBlock(BLOCK)
                .setOperand(operand)
                .build()
        ).unwrap()

        MIFARE.commitCounterValue(Commit.CommitCounter.newBuilder().setDstBlock(BLOCK).build())

        val readCounterValue = MIFARE.getCounterValue(
            Get.GetCounter.newBuilder()
                .setSrcBlock(BLOCK)
                .build()
        ).unwrap()

        assertEquals(value + operand, readCounterValue)
    }

    companion object {

        private val TCP_TRANSFER = IRTransfer(Transport.tcp(TCPParams("10.42.0.2", 42616)))
        private val CONTACTLESS_L1 = ContactlessL1(TCP_TRANSFER)
        private val MIFARE = Mifare(TCP_TRANSFER)
        private val CONTACT = ContactL1(TCP_TRANSFER)

        private val SAM_SLOT = CardSlotOuterClass.CardSlot.SAM3_SLOT
        private const val SAM_KEY_NUMBER = 2
        private const val SECTOR_NUMBER = 8
        private const val BLOCK = 1
        private val AV2_ARGS = Args.AuthenticationArguments.newBuilder()
            .setSlot(SAM_SLOT)
            .setKeyNumber(SAM_KEY_NUMBER)
            .build()
        private val AUTH_ON_SAM_KEY = Auth.SamKey.newBuilder()
            .setSectorNumber(SECTOR_NUMBER)
            .setKeyType(Auth.KeyType.TYPE_B)
            .setAv2Args(AV2_ARGS)
            .build()
    }
}
