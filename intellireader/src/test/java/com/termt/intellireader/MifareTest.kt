/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader

import com.google.protobuf.ByteString
import com.termt.intellireader.api.Transport
import com.termt.intellireader.entities.TCPParams
import com.termt.intellireader.modules.ContactlessL1
import com.termt.intellireader.modules.Mifare
import com.termt.intellireader.transfer.IRTransfer
import contactless.poll.PollForTokenOuterClass
import junit.framework.Assert.assertEquals
import mifare.classic.auth.Auth
import mifare.classic.counter.commit.Commit
import mifare.classic.counter.get.Get
import mifare.classic.counter.modify.Modify
import mifare.classic.counter.set.Set
import mifare.classic.read.Read
import mifare.classic.write.Write
import org.junit.Assert.assertArrayEquals
import org.junit.Before
import org.junit.Test

class MifareTest {

    @Before
    fun setUp() {
        CONTACTLESS_L1.pollForToken(PollForTokenOuterClass.PollForToken.newBuilder().setTimeout(15_000).build()).unwrap()
        MIFARE.authOnClearKey(AUTH_ON_CLEAR_KEY).unwrap()
    }

    @Test
    fun writeTest() {
        val data = ByteArray(16) { 0x37 }
        val blockCount = 1

        MIFARE.write(
            Write.WriteBlocks.newBuilder()
                .setStartBlock(BLOCK)
                .setData(ByteString.copyFrom(data))
                .build()
        ).unwrap()

        val readData = MIFARE.read(
            Read.ReadBlocks.newBuilder()
                .setStartBlock(BLOCK)
                .setBlockCount(blockCount)
                .build()
        ).unwrap()

        assertArrayEquals(data, readData)
    }

    @Test
    fun setCounterTest() {
        val value = 134

        MIFARE.setCounterValue(
            Set.SetCounter.newBuilder()
                .setDstBlock(BLOCK)
                .setValue(value)
                .build()
        ).unwrap()

        val readCounterValue = MIFARE.getCounterValue(
            Get.GetCounter.newBuilder()
                .setSrcBlock(BLOCK)
                .build()
        ).unwrap()

        assertEquals(value, readCounterValue)
    }

    @Test
    fun modifyCounterTest() {
        val value = 121
        val operand = -3

        MIFARE.setCounterValue(
            Set.SetCounter.newBuilder()
                .setDstBlock(BLOCK)
                .setValue(value)
                .build()
        ).unwrap()

        MIFARE.modifyCounterValue(
            Modify.ModifyCounter.newBuilder()
                .setSrcBlock(BLOCK)
                .setOperand(operand)
                .build()
        ).unwrap()

        MIFARE.commitCounterValue(Commit.CommitCounter.newBuilder().setDstBlock(BLOCK).build())

        val readCounterValue = MIFARE.getCounterValue(
            Get.GetCounter.newBuilder()
                .setSrcBlock(BLOCK)
                .build()
        ).unwrap()

        assertEquals(value + operand, readCounterValue)
    }

    companion object {

        private val TCP_TRANSFER = IRTransfer(Transport.tcp(TCPParams("10.0.2.73", 42616)))
        private val CONTACTLESS_L1 = ContactlessL1(TCP_TRANSFER)
        private val MIFARE = Mifare(TCP_TRANSFER)

        private const val SECTOR_NUMBER = 1
        private const val BLOCK = 1
        private val AUTH_ON_CLEAR_KEY = Auth.ClearKey.newBuilder()
            .setSectorNumber(SECTOR_NUMBER)
            .setKeyType(Auth.KeyType.TYPE_A)
            .setClearKey(ByteString.copyFrom(ByteArray(6) { 0xFF.toByte() }))
            .build()
    }
}
