/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.intellireader

import com.google.protobuf.ByteString
import com.termt.intellireader.api.Transport
import com.termt.intellireader.entities.IRCommand
import com.termt.intellireader.entities.TCPParams
import com.termt.intellireader.modules.ContactlessL2
import com.termt.intellireader.modules.Miscellaneous
import com.termt.intellireader.modules.Unspecified
import com.termt.intellireader.transfer.IRTransfer
import contactless.transaction.Transaction
import contactless.transaction.Transaction.PerformTransaction
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import misc.leds.LedsOuterClass
import org.junit.After
import org.junit.Before
import org.junit.Test

class EMVTest {

    @Before
    fun setup() {
        MISCELLANEOUS.setLeds(WAIT_FOR_CARD_LEDS).unwrap()
    }

    @After
    fun finish() {
        MISCELLANEOUS.setLeds(DISABLE_ALL_LEDS).unwrap()
    }

    @Test
    fun emvCardReaderTest() {
        val result = CONTACTLESS_L2.performEMVTransaction(TRANSACTION_PARAMS).unwrap()
        assertTrue("invalid emv transaction status: ${result.status.name}",
            result.status in listOf(
                Transaction.TransactionStatus.OFFLINE_APPROVED,
                Transaction.TransactionStatus.ONLINE_AUTHORIZATION_REQUIRED
            )
        )
    }

    @Test
    fun cancelOperationTest() {
        val startEMVTransactionResult = CONTACTLESS_L2.startEMVTransaction(TRANSACTION_PARAMS).next()
        assertEquals(IRCommand.Type.Pending, startEMVTransactionResult.type)
        UNSPECIFIED.cancel()
    }

    companion object {

        private val TCP_TRANSFER = IRTransfer(Transport.tcp(TCPParams("localhost", 7380)))
        private val CONTACTLESS_L2 = ContactlessL2(TCP_TRANSFER)
        private val MISCELLANEOUS = Miscellaneous(TCP_TRANSFER)
        private val UNSPECIFIED = Unspecified(TCP_TRANSFER)

        private val TRANSACTION_PARAMS : PerformTransaction = PerformTransaction.newBuilder()
            .setAmountAuthorized(100)
            .setTransactionType(ByteString.copyFrom(byteArrayOf(0x00)))
            .setTransactionDate(ByteString.copyFrom(byteArrayOf(0x13, 0x06, 0x13)))
            .setTransactionTime(ByteString.copyFrom(byteArrayOf(0x12, 0x00, 0x00)))
            .setTerminalCountryCode(ByteString.copyFrom(byteArrayOf(0x06, 0x48)))
            .setTransactionCurrencyCode(ByteString.copyFrom(byteArrayOf(0x06, 0x48)))
            .build()

        private val WAIT_FOR_CARD_LEDS = LedsOuterClass.Leds.newBuilder()
            .setBlue(true)
            .setRed(false)
            .setYellow(false)
            .setGreen(false)
            .build()

        private val DISABLE_ALL_LEDS = LedsOuterClass.Leds.newBuilder()
            .setBlue(false)
            .setRed(false)
            .setYellow(false)
            .setGreen(false)
            .build()
    }
}
