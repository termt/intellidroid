/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.irdemo

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText

class InitActivity : AppCompatActivity() {

    private lateinit var ipEdit: AppCompatEditText
    private lateinit var portEdit: AppCompatEditText
    private lateinit var startButton: AppCompatButton
    private lateinit var countSeeker: SeekBar
    private lateinit var sp: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tcp_params_layout)

        ipEdit = findViewById(R.id.ip_edit)
        portEdit = findViewById(R.id.port_edit)
        startButton = findViewById(R.id.start_button)
        countSeeker = findViewById(R.id.count_seek_bar)

        sp = getSharedPreferences(TCP_SP, 0)

        ipEdit.setText(sp.getString(IP_PREF, ""))
        portEdit.setText(sp.getInt(PORT_PREF, 42616).toString())
        countSeeker.setProgress(sp.getInt(COUNT_PREF, 0))

        startButton.setOnClickListener {
            val ip = ipEdit.editableText.toString()
            val port = portEdit.editableText.toString().toIntOrNull()
            val count = countSeeker.progress

            when {
                ip.isBlank() -> {
                    ipEdit.error = "IP-address can't be empty"
                    ipEdit.requestFocus()
                }

                port == null -> {
                    portEdit.error = "Invalid port value"
                    portEdit.requestFocus()
                }
                else -> {
                    sp.edit()
                        .putString(IP_PREF, ip)
                        .putInt(PORT_PREF, port)
                        .putInt(COUNT_PREF, count)
                        .apply()

                    val intent = Intent(this@InitActivity, MainActivity::class.java)
                    intent.putExtra(MainActivity.MODE, MainActivity.TCP_MODE)
                    intent.putExtra(MainActivity.IP, ip)
                    intent.putExtra(MainActivity.PORT, port)
                    intent.putExtra(MainActivity.COUNT, count)
                    startActivity(intent)
                }
            }
        }
    }

    private companion object {

        private const val TCP_SP = "tcp_prefs"
        private const val IP_PREF = "ip_pref"
        private const val PORT_PREF = "port_pref"
        private const val COUNT_PREF = "count_pref"
    }
}
