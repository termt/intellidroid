/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.irdemo

import android.app.Application
import com.termt.irdemo.data.TransactionDataBase
import com.termt.irdemo.data.TransactionDataDao

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        dataSource = TransactionDataBase.getInstance(INSTANCE).transactionDataDao

    }

    companion object {
        lateinit var INSTANCE: Application
        lateinit var dataSource : TransactionDataDao
    }
}
