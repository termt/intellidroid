package com.termt.irdemo.connection

object ConnectionData {
    var quality : Int = 0
    var onlineCheckSuccess : Int = 0
    var downloadCheckSuccess : Int = 0
    override fun toString(): String {
        return "{ " +
                "\"quality\": $quality, " +
                "\"onlinePass\": $onlineCheckSuccess, " +
                "\"downloadPass\": $downloadCheckSuccess " +
                "}"
    }
}