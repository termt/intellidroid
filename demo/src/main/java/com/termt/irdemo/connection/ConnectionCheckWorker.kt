package com.termt.irdemo.connection

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters

class ConnectionCheckWorker(appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params) {

    override suspend fun doWork(): Result {
        return if (ConnectionCheck.check()) Result.success()
        else Result.failure()
    }

    companion object {
        const val WORK_NAME = "com.termt.irdemo.connection.ConnectionCheckWorker"
        private val TAG = ConnectionCheckWorker::class.java.simpleName
    }
}