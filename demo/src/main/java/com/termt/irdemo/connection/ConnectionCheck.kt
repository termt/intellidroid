package com.termt.irdemo.connection

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.termt.irdemo.App
import com.termt.irdemo.BuildConfig
import kotlinx.coroutines.suspendCancellableCoroutine

object ConnectionCheck {

    private val TAG = ConnectionCheck::class.java.simpleName
    private const val url = "https://gist.githubusercontent.com/khaykov/" +
            "a6105154becce4c0530da38e723c2330/raw/" +
            "41ab415ac41c93a198f7da5b47d604956157c5c3/gistfile1.txt"

    suspend fun check(): Boolean {
        ConnectionData.quality += 1
        val out = isOnline() && isDownloading()
        Log.d(TAG, ConnectionData.toString())
        return out
    }

    private fun isOnline(): Boolean {
        val connectivityManager =
            App.INSTANCE.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                // Vendotek operates on Android 8.1 ( Build.VERSION_CODES.O_MR1 = 27 > Build.VERSION_CODES.M = 23 )
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                } else {
                    null
                }
            if (capabilities != null &&
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                ConnectionData.onlineCheckSuccess += 1
                return true
            }
        }
        return false
    }

    private suspend fun isDownloading(): Boolean {
        return suspendCancellableCoroutine { continuation ->
            val queue = Volley.newRequestQueue(App.INSTANCE)
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                { response ->
                    // Display the first 500 characters of the response string.
                    if (BuildConfig.DEBUG) Log.d(TAG, "Response is ${response.substring(0, 500)}")
                    ConnectionData.downloadCheckSuccess += 1
                    continuation.resumeWith(Result.success(true))
                },
                { continuation.cancel(Exception("Volley Error")) })
            queue.add(stringRequest)
        }
    }
}