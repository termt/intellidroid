package com.termt.irdemo.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [TransactionData::class], version = 1, exportSchema = false)
abstract class TransactionDataBase : RoomDatabase() {

    abstract val transactionDataDao: TransactionDataDao

    companion object {

        @Volatile
        private var INSTANCE: TransactionDataBase? = null

        fun getInstance(context: Context): TransactionDataBase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        TransactionDataBase::class.java,
                        "transactions_table"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
