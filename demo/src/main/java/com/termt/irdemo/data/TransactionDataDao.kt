package com.termt.irdemo.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.termt.irdemo.data.TransactionData

@Dao
interface TransactionDataDao {

    @Insert
    fun insert(transaction: TransactionData)

    @Update
    fun update(transaction: TransactionData)

    @Query ("SELECT * from transactions_table WHERE transactionId = :key")
    fun get(key: Long) : TransactionData?

    @Query ("DELETE FROM transactions_table")
    fun clear()

    @Query ( "SELECT * FROM transactions_table ORDER BY transactionId DESC LIMIT 1")
    fun getLast() : TransactionData?

    @Query ( "SELECT * FROM transactions_table WHERE device_serial = :serial")
    fun getBySerial(serial: String) : List<TransactionData>

    @Query ( "SELECT * FROM transactions_table WHERE device_address = :address")
    fun getByAddress(address: String) : List<TransactionData>

    @Query ( "SELECT DISTINCT device_serial FROM transactions_table")
    fun getUniqueSerials() : List<String?>

    @Query ( "SELECT DISTINCT device_address FROM transactions_table")
    fun getUniqueAddresses() : List<String?>

    @Query("SELECT * FROM transactions_table ORDER BY transactionId DESC")
    fun getAllTransactions(): LiveData<List<TransactionData>>

    @Query("SELECT * FROM transactions_table ORDER BY transactionId DESC")
    fun getAll(): List<TransactionData>
}