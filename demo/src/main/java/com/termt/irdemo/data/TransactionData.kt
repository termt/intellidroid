package com.termt.irdemo.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.termt.intellireader.entities.IRCommand

@Entity(tableName = "transactions_table")
data class TransactionData (
    @PrimaryKey(autoGenerate = true)
    var transactionId : Long = 0L,

    @ColumnInfo(name = "start_time_milli")
    var startTimeMilli: Long = System.currentTimeMillis(),

    @ColumnInfo(name = "device_serial")
    var serial: String? = null,

    @ColumnInfo(name = "device_address")
    var address: String? = null,

    @ColumnInfo(name = "transport_name")
    var transport: String? = null,

    @ColumnInfo(name = "status_name")
    var status: String? = null,

    @ColumnInfo(name = "masked_pan")
    var pan: String? = null
)