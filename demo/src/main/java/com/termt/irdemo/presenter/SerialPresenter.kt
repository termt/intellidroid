/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.irdemo.presenter
import android.util.Log
import com.termt.intellireader.api.Transport
import com.termt.intellireader.transport.IRTransport
import com.termt.irdemo.presenter.AbstractPresenter
import kotlinx.coroutines.runBlocking

class SerialPresenter(private val address: String) : AbstractPresenter() {

    override fun getAddress(): String {
        return address;
    }

    override fun provideTransport(): IRTransport? {
        Log.d("irdemo","provide serial transport")
        return runBlocking {
                    Transport.serial(address, 115200, 10000)
            }
        }
}
