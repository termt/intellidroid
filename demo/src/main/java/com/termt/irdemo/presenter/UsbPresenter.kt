/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.irdemo.presenter

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.util.Log
import com.termt.intellireader.api.Transport
import com.termt.intellireader.transport.IRTransport
import com.termt.irdemo.App
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class UsbPresenter : AbstractPresenter() {

    private val usbManager = App.INSTANCE.getSystemService(Context.USB_SERVICE) as UsbManager
    override fun getAddress(): String {
        return "USB"
    }

    override fun provideTransport(): IRTransport? {
        for ( d in usbManager.deviceList.values)
        {
            Log.d("DEMO_USB", "USBDevice " + d.productName);
        }

        var usbDevice = usbManager.deviceList.values.firstOrNull { it.productName?.contains("FT232", ignoreCase = true) ?: false }

        if (usbDevice == null)
            usbDevice = usbManager.deviceList.values.firstOrNull { it.productName?.contains("DCSD", ignoreCase = true) ?: false }

        if (usbDevice == null)
            usbDevice = usbManager.deviceList.values.firstOrNull { it.productName?.contains("USB Serial", ignoreCase = true) ?: false }

        return runBlocking {
            if (usbDevice == null) {
                view?.showResult("USB device not found")
                null
            } else {
                if (requestPermissionForDevice(usbDevice)) {
                    Log.d("DEMO_USB", "USBDevice will use" + usbDevice.productName);
                    Transport.usb(usbDevice, App.INSTANCE)
                } else {
                    view?.showResult("Permission for USB not granted")
                    null
                }
            }
        }
    }

    private suspend fun requestPermissionForDevice(device: UsbDevice): Boolean =
        suspendCoroutine { cont ->
            val receiver = object : BroadcastReceiver() {
                var received = false

                override fun onReceive(context: Context, intent: Intent) {
                    if (!received) {
                        received = true

                        if (intent.action == ACTION_USB_PERMISSION) {
                            cont.resume(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false))
                        } else {
                            cont.resume(false)
                        }
                    }
                }
            }
            App.INSTANCE.registerReceiver(receiver, IntentFilter(ACTION_USB_PERMISSION))

            val pendingIntent = PendingIntent.getBroadcast(
                App.INSTANCE, 0, Intent(
                    ACTION_USB_PERMISSION
                ), 0)
            usbManager.requestPermission(device, pendingIntent)
        }

    companion object {

        private const val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"
    }
}
