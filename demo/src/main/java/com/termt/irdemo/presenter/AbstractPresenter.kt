/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.irdemo.presenter

import android.util.Log
import com.google.protobuf.ByteString
import com.termt.intellireader.api.errors.IRMessageIDMismatchException
import com.termt.intellireader.entities.TransferResult
import com.termt.intellireader.modules.*
import com.termt.intellireader.transfer.IRTransfer
import com.termt.intellireader.transfer.IRTransferConenction
import com.termt.intellireader.transport.IRTransport
import com.termt.irdemo.App
import com.termt.irdemo.MainView
import com.termt.irdemo.data.TransactionData
import common.failure.Failure
import contactless.poll.PollForTokenOuterClass
import contactless.transaction.Transaction
import gui.menu_dialog.MenuDialogOuterClass
import gui.picture_id.PictureIdOuterClass
import kotlinx.coroutines.*
import misc.buzzer.Buzzer
import misc.device.Device
import misc.leds.LedsOuterClass
import kotlin.concurrent.thread

abstract class AbstractPresenter {

    abstract fun getAddress() : String

    private val dao = App.dataSource
    private lateinit var job: Job
    protected var view: MainView? = null
    private var transfer: IRTransferConenction? = null
    private var errors: Int = 0

    fun onStart(view: MainView) {
        Log.w("THREAD", "onStart, launching")

        this.view = view
        job = Job()
        start()
    }

    fun onStop() {
        Log.w("THREAD", "onStop, stopping")
        job.cancel()
        transfer?.stop()
        view = null
    }



    private fun start() {
        job = CoroutineScope(Dispatchers.IO).launch {
            view?.startProgress()
            while (isActive) {
                Log.w("THREAD", "alive! doing stuff")
                try {
                    performEMVAsync()
                } catch (ex: Exception) {
                    view?.showResult(ex.message ?: ex.javaClass.name)
                } finally {
                    //view?.startAction("Error, waiting")
                    delay(4000)
                    //view?.startAction("Error, waiting f")
                }
            }
            view?.stopProgress()
        }
    }

    protected abstract fun provideTransport(): IRTransport?
    fun ByteArray.toHex(): String = joinToString(separator = "") { eachByte -> "%02x".format(eachByte) }

    private fun performEMVAsync() {
        if (transfer == null) {
            view?.startAction("Initializing")
            Log.d("irDemo", "INIT")
            val transport = provideTransport()
            Log.d("irDemo", "transport " + transport.toString())
            if (transport == null) {
                view?.showResult("Failed to init transport")
                Log.d("irDemo", "transport null")
                return
            }

            view?.startAction("transfer upd")
            transfer = IRTransfer(transport)
        }

        if (!performTransaction(transfer!!))
            transfer = null
    }

    private fun performTransaction(transfer: IRTransferConenction) : Boolean{

        view?.showResult("")

        view?.startAction("Do cancel")
        if (!cancelTransaction(transfer))
            return false

        view?.startAction("Do LEDs")
        blinkLEDs(transfer)

        val transport = provideTransport()
        Log.d("irDemo2", "transport " + transport.toString())
        if (transport == null) {
            view?.showResult("Failed to init transport")
            Log.d("irDemo2", "transport null")
            return false;
        }

        view?.startAction("transfer upd")
        val ntransfer = IRTransfer(transport)

        while (job.isActive) {
            //request amount

            view?.startAction("Tap card")

            val res =  ContactlessL1(transfer).pollForTokenLow(
                PollForTokenOuterClass.PollForToken.newBuilder()
                    .setTimeout(5000)
                    .setPreferMifare(true)
                    .build());
            if (res is TransferResult.Error && res.failure.error == Failure.Error.TIMEOUT_EXPIRED) {

                view?.startAction("(timeout)")
                Thread.sleep(1000)
                continue
            }

            if (res is TransferResult.Error ) {

                view?.startAction("(tap error)")
                Thread.sleep(1000)
                continue
            }

            else
            {

                view?.showResult("card tapped " + res.unwrap().type + " " + res.unwrap().id.toByteArray().toHex());
                //Miscellaneous(ntransfer).makeSound(arrayListOf(Buzzer.Note.newBuilder().setFrequencyHz(800).setDurationMs(100).build()))
                Miscellaneous(transfer).makeSound(arrayListOf(Buzzer.Note.newBuilder().setFrequencyHz(800).setDurationMs(100).build()))
                Thread.sleep(100)
                Miscellaneous(ntransfer).makeSound(arrayListOf(Buzzer.Note.newBuilder().setFrequencyHz(800).setDurationMs(100).build()))
            }

            ContactlessL1(transfer).fieldPowerOff();

            break
        }

        return true
    }

    private fun buildDataEntry(transfer: IRTransferConenction, status: String): TransactionData {
        return TransactionData(
            address = getAddress(),
            serial = deviceInfo(transfer = transfer).serialNumber,
            transport = transfer.toString(),
            status = status)
    }

    private fun insert(transactionData: TransactionData) {
        dao.insert(transactionData)
    }

    private fun cancelTransaction(transfer: IRTransferConenction): Boolean {
        return try {
            Unspecified(transfer).cancel()
            true
        } catch (ex: IRMessageIDMismatchException) {
            false
        }
    }

    private fun idleLEDs(transfer: IRTransferConenction):Boolean {
        return Miscellaneous(transfer).setLeds(
            LedsOuterClass.Leds.newBuilder()
                .setBlue(true)
                .setGreen(false)
                .setRed(false)
                .setYellow(false)
                .build()
        ) is TransferResult.Success
    }

    private fun blinkLEDs(transfer: IRTransferConenction):Boolean {

        for ( i in 1..10 )
        {
            Miscellaneous(transfer).setLeds(
                LedsOuterClass.Leds.newBuilder()
                    .setBlue(true)
                    .setGreen(false)
                    .setRed(false)
                    .setYellow(false)
                    .build()
            )
            Thread.sleep(50)
            Miscellaneous(transfer).setLeds(
                LedsOuterClass.Leds.newBuilder()
                    .setBlue(false)
                    .setGreen(true)
                    .setRed(false)
                    .setYellow(false)
                    .build()
            )
            Thread.sleep(50)
            Miscellaneous(transfer).setLeds(
                LedsOuterClass.Leds.newBuilder()
                    .setBlue(false)
                    .setGreen(false)
                    .setRed(true)
                    .setYellow(false)
                    .build()
            )
            Thread.sleep(50)
            Miscellaneous(transfer).setLeds(
                LedsOuterClass.Leds.newBuilder()
                    .setBlue(false)
                    .setGreen(false)
                    .setRed(false)
                    .setYellow(true)
                    .build()
            )
            Thread.sleep(50)
        }
        return true;
    }

    private fun disableLEDs(transfer: IRTransferConenction) : Boolean {
        return Miscellaneous(transfer).setLeds(
            LedsOuterClass.Leds.newBuilder()
                .setBlue(false)
                .setGreen(false)
                .setRed(false)
                .setYellow(false)
                .build()
        ) is TransferResult.Success
    }

    private fun deviceInfo(transfer: IRTransferConenction) : Device.DeviceInfo {
        return Miscellaneous(transfer).readDeviceInfo().unwrap()
    }

    private fun showAmountDialog(transfer: IRTransferConenction): Int {
        val iList = MenuDialogOuterClass.ItemList.newBuilder()
            .addItems(MenuDialogOuterClass.Item.newBuilder().setText("10").setLeafItem(MenuDialogOuterClass.LeafItem.newBuilder().setId(1000)))
            .addItems(MenuDialogOuterClass.Item.newBuilder().setText("20").setLeafItem(MenuDialogOuterClass.LeafItem.newBuilder().setId(2000)))
            .addItems(MenuDialogOuterClass.Item.newBuilder().setText("30").setLeafItem(MenuDialogOuterClass.LeafItem.newBuilder().setId(3000)))
            .addItems(MenuDialogOuterClass.Item.newBuilder().setText("Input yourself").setLeafItem(MenuDialogOuterClass.LeafItem.newBuilder().setId(0)))
            //
            .build()

        val result = Gui(transfer).showMenu("Amount", iList, 100000)
        if (result is TransferResult.Success
            && result.unwrap().hasId())
                return result.unwrap().id
        return -1
    }


    private fun requestAmountInput(transfer: IRTransferConenction): Int {
        val result = Gui(transfer).getUserInput("Input amount", "XXXXXX", 100000)

        if (result is TransferResult.Success) {
                return result.unwrap().text.toInt()
        }
        return  -1
    }

    private fun showTapCardImage(transfer: IRTransferConenction): Boolean {
        return Gui(transfer).showScreen(PictureIdOuterClass.PictureId.EMV_CONTACTLESS_SYMBOL) is TransferResult.Success
    }

    private fun showTapCardImageWithText(transfer: IRTransferConenction, text: String): Boolean {
        return Gui(transfer).showText(text, PictureIdOuterClass.PictureId.EMV_CONTACTLESS_SYMBOL) is TransferResult.Success
    }

    private fun showTimeoutImage(transfer: IRTransferConenction): Boolean {
        return Gui(transfer).showScreen(PictureIdOuterClass.PictureId.AWAITING) is TransferResult.Success
    }

    private fun showRejectedImage(transfer: IRTransferConenction): Boolean {
        return Gui(transfer).showScreen(PictureIdOuterClass.PictureId.REJECTED) is TransferResult.Success
    }

    private fun showAcceptedImage(transfer: IRTransferConenction): Boolean {
        return Gui(transfer).showScreen(PictureIdOuterClass.PictureId.APPROVED) is TransferResult.Success
    }


    companion object {

        private val TRANSACTION_PARAMS = Transaction.PerformTransaction.newBuilder()
            .setPollForToken(PollForTokenOuterClass.PollForToken.newBuilder().setTimeout(5000).build())
            .setAmountAuthorized(100)
            .setTransactionType(ByteString.copyFrom(byteArrayOf(0x00)))
            .setTransactionDate(ByteString.copyFrom(byteArrayOf(0x13, 0x06, 0x13)))
            .setTransactionTime(ByteString.copyFrom(byteArrayOf(0x12, 0x00, 0x00)))
            .setTerminalCountryCode(ByteString.copyFrom(byteArrayOf(0x06, 0x48)))
            .setTransactionCurrencyCode(ByteString.copyFrom(byteArrayOf(0x06, 0x48)))
            .build()
    }
}
