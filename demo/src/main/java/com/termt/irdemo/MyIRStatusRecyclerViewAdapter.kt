package com.termt.irdemo


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView


import com.termt.irdemo.presenter.AbstractPresenter


import kotlinx.android.synthetic.main.fragment_irstatus.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyIRStatusRecyclerViewAdapter(
        private val mValues: List<AbstractPresenter>
     )
    : RecyclerView.Adapter<MyIRStatusRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_irstatus, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mIdView.text = item.getAddress()

        with(holder.mView) {
            tag = item
        }

        item.onStart(holder)
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView), MainView {
        override fun stopProgress() {
            mProgressBar.isVisible = false
        }

        override fun startProgress() {
            mProgressBar.isVisible = true
        }

        val mIdView: TextView = mView.addr_view
        val mContentView: TextView = mView.status_view
        val mActionView: TextView = mView.action_view
        val mProgressBar: ProgressBar = mView.progress_bar

        override fun showResult(status: String) {
            GlobalScope.launch { withContext(Dispatchers.Main) { mContentView.text = status }}

        }

        override fun startAction(action: String) {
            GlobalScope.launch { withContext(Dispatchers.Main) { mActionView.text = action }}
        }

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
