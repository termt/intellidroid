/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.irdemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.termt.intellireader.entities.TCPParams
import com.termt.irdemo.presenter.AbstractPresenter
import com.termt.irdemo.presenter.SerialPresenter
import com.termt.irdemo.presenter.TCPPresenter
import com.termt.irdemo.presenter.UsbPresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*

//==============================================================================================
fun getProp(prop: String?): String {
    var process: Process? = null
    var bufferedReader: BufferedReader? = null
    val GETPROP_EXECUTABLE_PATH = "/system/bin/getprop"
    return try {
        process = ProcessBuilder().command(GETPROP_EXECUTABLE_PATH, prop).redirectErrorStream(true)
            .start()
        bufferedReader = BufferedReader(InputStreamReader(process.inputStream))
        var line = bufferedReader.readLine()
        if (line == null) {
            line = "" //prop not set
        }
        line
    } catch (e: Exception) {
        ""
    } finally {
        if (bufferedReader != null) {
            try {
                bufferedReader.close()
            } catch (e: IOException) {
            }
        }
        process?.destroy()
    }
}

class MainActivity : AppCompatActivity(), CoroutineScope by MainScope () {

    private var presenters: LinkedList <AbstractPresenter> = LinkedList()

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        presenters.forEach { it.onStop() }
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*for (ip in arrayOf("10.0.2.95","10.0.2.159", "10.0.2.146", "10.0.2.116", "10.0.2.156", "10.0.2.92"))
        {
            presenters.add(TCPPresenter(TCPParams(ip, intent.getIntExtra(PORT, -1))))
        }*/

        for (i in 0..intent.extras!!.getInt(COUNT)) {
           val presenter = when (intent.extras!!.getInt(MODE)) {
                USB_MODE -> UsbPresenter()
                TCP_MODE -> TCPPresenter(TCPParams(incrementIP(intent.getStringExtra(IP)!!, i), intent.getIntExtra(PORT, -1)))
                SERIAL_MODE -> {
                    val default_addr: String = getProp("persist.sys.irdev");
                    if (default_addr.isEmpty())
                    {
                        default_addr == "/dev/ttyUSB3";
                    }
                    Log.d("MTAG","Device addr: " + default_addr);
                    SerialPresenter(default_addr);
                }
                else -> throw IllegalStateException("unknown mode: ${intent.extras!!.getInt(MODE)}")
            }

            presenters.add(presenter)
        }

        val view = findViewById<RecyclerView>(R.id.fragment)
        view.adapter  = MyIRStatusRecyclerViewAdapter(presenters)
    }

    private fun incrementIP(ip: String, i: Int): String {
        var last_octet = ip.split(".").last().toInt()
        last_octet+=i
        return ip.split(".").subList(0,3).joinToString(".") + "." + last_octet.toString();
    }

    companion object {

        const val MODE = "mode"

        const val IP = "ip"
        const val ADDRESS = "address"
        const val PORT = "port"
        const val COUNT = "count"

        const val TCP_MODE = 0
        const val USB_MODE = 1
        const val SERIAL_MODE = 2
    }
}
