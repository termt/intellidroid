package com.termt.irdemo.data

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class TransactionsDataBaseTest {

    private lateinit var transactionDao: TransactionDataDao
    private lateinit var db: TransactionDataBase

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, TransactionDataBase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        transactionDao = db.transactionDataDao
    }

    @Test
    @Throws(Exception::class)
    fun insertDefaultAndGetLastTransaction() {
        transactionDao.insert(defaultTransaction)
        val getLastTransaction = transactionDao.getLast()
        assertEquals(getLastTransaction?.serial, null)
        assertEquals(getLastTransaction?.address, null)
        assertEquals(getLastTransaction?.transport, null)
        assertEquals(getLastTransaction?.status, null)
        assertEquals(getLastTransaction?.pan, null)
    }

    @Test
    @Throws(Exception::class)
    fun insertCustomAndGetLastTransaction() {
        transactionDao.insert(customTransaction)
        val getLastTransaction = transactionDao.getLast()
        assertEquals(getLastTransaction?.serial, "1111")
        assertEquals(getLastTransaction?.address, "10.0.0.2")
        assertEquals(getLastTransaction?.transport, "tcp")
        assertEquals(getLastTransaction?.status, "Undef")
        assertEquals(getLastTransaction?.pan, "****0000")
    }

    @Test
    @Throws(Exception::class)
    fun clearTransaction() {
        transactionDao.insert(defaultTransaction)
        transactionDao.clear()
        val getLastTransaction = transactionDao.getLast()
        assertEquals(getLastTransaction, null)
        val getListAllTransactions = transactionDao.getAll()
        assertEquals(getListAllTransactions.lastIndex, -1)
    }

    @Test
    @Throws(Exception::class)
    fun getAllTransactions() {
        transactionDao.clear()
        transactionDao.insert(defaultTransaction)
        transactionDao.insert(defaultTransaction)
        transactionDao.insert(defaultTransaction)
        transactionDao.insert(defaultTransaction)
        transactionDao.insert(defaultTransaction)
        val getListAllTransactions = transactionDao.getAll()
        assertEquals(getListAllTransactions.lastIndex, 4)
    }

    @Test
    @Throws(Exception::class)
    fun getUniqueAddressList() {
        transactionDao.clear()
        transactionDao.insert(defaultTransaction)
        transactionDao.insert(oneTransaction)
        transactionDao.insert(twoTransaction)
        transactionDao.insert(twoTransaction)
        transactionDao.insert(customTransaction)
        val getListUniqueAddress = transactionDao.getUniqueAddresses()
        assertEquals(getListUniqueAddress.lastIndex, uniqueAddressList.lastIndex)
        for (address in getListUniqueAddress) assertTrue(uniqueAddressList.contains(address))
    }

    @Test
    @Throws(Exception::class)
    fun getUniqueSerialList() {
        transactionDao.clear()
        transactionDao.insert(defaultTransaction)
        transactionDao.insert(oneTransaction)
        transactionDao.insert(twoTransaction)
        transactionDao.insert(twoTransaction)
        transactionDao.insert(customTransaction)
        val getListUniqueSerial = transactionDao.getUniqueSerials()
        assertEquals(getListUniqueSerial.lastIndex, uniqueAddressList.lastIndex)
        for (address in getListUniqueSerial) assertTrue(uniqueSerialsList.contains(address))
    }

    @Test
    @Throws(Exception::class)
    fun updateTransaction() {
        transactionDao.insert(defaultTransaction)
        val getLastTransaction = transactionDao.getLast()
        getLastTransaction?.serial = "updated"
        getLastTransaction?.address = "updated"
        getLastTransaction?.transport = "updated"
        getLastTransaction?.status = "updated"
        getLastTransaction?.pan = "updated"
        if (getLastTransaction != null) {
            transactionDao.update(getLastTransaction)
        }
        val getTransaction = transactionDao.get(getLastTransaction!!.transactionId)
        assertEquals(getTransaction?.serial, "updated")
        assertEquals(getTransaction?.address, "updated")
        assertEquals(getTransaction?.transport, "updated")
        assertEquals(getTransaction?.status, "updated")
        assertEquals(getTransaction?.pan, "updated")
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    companion object{
        private val defaultTransaction = TransactionData()
        private val customTransaction = TransactionData(
            serial = "1111", address = "10.0.0.2",
            transport = "tcp", status = "Undef",
            pan = "****0000"
        )
        private val oneTransaction = TransactionData(
            serial = "1", address = "1",
            transport = "1", status = "1",
            pan = "1"
        )
        private val twoTransaction = TransactionData(
            serial = "2", address = "2",
            transport = "2", status = "2",
            pan = "2"
        )
        private val uniqueAddressList = listOf(
            defaultTransaction.address,
            customTransaction.address,
            oneTransaction.address,
            twoTransaction.address
        )
        private val uniqueSerialsList = listOf(
            defaultTransaction.serial,
            customTransaction.serial,
            oneTransaction.serial,
            twoTransaction.serial
        )
    }
}