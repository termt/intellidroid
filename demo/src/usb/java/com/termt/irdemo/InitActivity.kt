/*
 * Copyright (C) 2019 Terminal Technologies Ltd
 *
 * RESTRICTIONS
 *
 * You may not sell, assign, sublicense, lease, rent or
 * distribute the Licensed Software, in whole or in part.
 *
 * Unless required by applicable law or agreed to in writing, software
 * is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.termt.irdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

class InitActivity : AppCompatActivity() {
    override fun onStart() {
        super.onStart()
        val intent = Intent(this@InitActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(MainActivity.MODE, MainActivity.USB_MODE)
        finish()
        startActivity(intent)
    }
}
