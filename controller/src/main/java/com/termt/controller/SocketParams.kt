package com.termt.controller

import org.json.JSONArray
import org.json.JSONObject

object SocketParams {
    var ip: String = ""
    var port: Int = 0
    var jsonConnections : JSONObject = JSONObject()
    var jsonTransactions : JSONArray = JSONArray()

    fun returnStringList() : List<String> {
        val initialList = mutableListOf(jsonConnections.toString())
        for (i in 0 until jsonTransactions.length()) {
            initialList.add(jsonTransactions.getJSONObject(i).toString())
        }
        return initialList
    }
}