package com.termt.controller


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class SocketStatsAdapter(private val onClick: (String) -> Unit) :
    ListAdapter<String, SocketStatsAdapter.StatsViewHolder>(SocketDiffCallback) {

    /* ViewHolder for socketData, takes in the inflated view and the onClick behavior. */
    class StatsViewHolder(itemView: View, val onClick: (String) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val statsTextView: TextView = itemView.findViewById(R.id.json)
        private var currentString: String? = null

        init {
            itemView.setOnClickListener {
                currentString?.let {
                    onClick(it)
                }
            }
        }

        fun bind(data: String) {
            currentString = data
            statsTextView.text = data
        }
    }

    /* Creates and inflates view and return SocketDataViewHolder. */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view, parent, false)
        return StatsViewHolder(view, onClick)
    }

    /* Gets current data and uses it to bind view. */
    override fun onBindViewHolder(holder: StatsViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data)

    }
}

object SocketDiffCallback : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }
}