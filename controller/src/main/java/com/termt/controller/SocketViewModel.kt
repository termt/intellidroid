package com.termt.controller

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SocketViewModel(val dataSource: DataSource) : ViewModel(){

    val socketLiveData = dataSource.getList()

    fun updateData() {
        dataSource.updateList(SocketParams.returnStringList())
    }

}

class SocketViewModelFactory() : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SocketViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SocketViewModel(
                dataSource = DataSource.getDataSource()
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}