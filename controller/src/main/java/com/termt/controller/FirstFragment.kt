package com.termt.controller

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.termt.controller.databinding.FragmentFirstBinding
import java.lang.Integer.parseInt

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonFirst.setOnClickListener {
            setConnectionSettings()
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
    }

    private fun setConnectionSettings() {

        val ip = activity?.findViewById(R.id.ip_edit) as EditText
        val port = activity?.findViewById(R.id.port_edit) as EditText

        SocketParams.ip =
            if (ip.text.toString().trim().isNullOrBlank())
                resources.getString(R.string.ip_default)
            else ip.text.toString().trim()
        SocketParams.port =
            if (port.text.toString().trim().isNullOrBlank())
                parseInt(resources.getString(R.string.port_default))
            else parseInt(port.text.toString().trim())

        if (BuildConfig.DEBUG) Log.d(TAG,"${SocketParams.ip} : ${SocketParams.port}")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object{
        private val TAG = FirstFragment::class.java.simpleName
    }
}