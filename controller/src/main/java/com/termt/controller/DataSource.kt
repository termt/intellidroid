package com.termt.controller

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class DataSource {

    private var initialList = SocketParams.returnStringList()
    private val liveData = MutableLiveData(initialList)

    fun getList(): LiveData<List<String>> {
        Log.d("DataSource",initialList.toString())
        Log.d("DataSource jsonConnections", SocketParams.jsonConnections.toString())
        Log.d("DataSource jsonConnections", SocketParams.jsonTransactions.toString())
        return liveData
    }

    fun updateList(dataList: List<String>){

        val currentList = liveData.value

        if (currentList == null || currentList.size == 1) {
            liveData.postValue(dataList)
        } else {
            val updatedList = currentList.toMutableList()
            updatedList[0] = dataList[0]
            if (dataList.size > currentList.size) {
                for (i in IntRange(currentList.lastIndex, dataList.lastIndex)){
                    updatedList.add(dataList[i])
                }
                liveData.postValue(updatedList)
            }
        }
    }

    companion object {

        private var INSTANCE: DataSource? = null

        fun getDataSource(): DataSource {
            return synchronized(DataSource::class) {
                val newInstance = INSTANCE ?: DataSource()
                INSTANCE = newInstance
                newInstance
            }
        }
    }
}