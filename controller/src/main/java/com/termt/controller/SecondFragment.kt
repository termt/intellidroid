package com.termt.controller

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.termt.controller.databinding.FragmentSecondBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private val fragmentScope = CoroutineScope(Dispatchers.IO)
    private val socketViewModel by viewModels<SocketViewModel> {
        SocketViewModelFactory()
    }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentScope.launch { Socket.run() }

        _binding = FragmentSecondBinding.inflate(inflater, container, false)

        val adapter = SocketStatsAdapter{ string -> adapterOnClick(string) }
        val recyclerView: RecyclerView = _binding!!.root.findViewById(R.id.recycler_view)
        recyclerView.adapter = adapter
        socketViewModel.socketLiveData.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it as MutableList<String>)
            }
        })
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSecond.setOnClickListener {
            fragmentScope.launch { Socket.run() }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun adapterOnClick(json: String) {
        Log.d(TAG, json)
    }

    companion object{
        private val TAG = SecondFragment::class.java.simpleName
    }
}