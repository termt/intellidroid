package com.termt.controller

import android.util.Log
import com.termt.controller.SocketParams.ip
import com.termt.controller.SocketParams.port
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.DataInputStream
import java.lang.Exception
import java.net.Socket

object Socket: Thread() {

    private val TAG = Socket::class.java.simpleName
    private const val delimiter = "@"
    private const val tagConnection = "Connections"
    private const val tagTransaction = "Transactions"

    override fun run() {
        try {
            val socket = Socket(ip, port)
            val dataInputStream = DataInputStream(socket.getInputStream())
            val input = BufferedInputStream(dataInputStream).readBytes().toString(Charsets.UTF_8)
            parseMessage(input)
            dataInputStream.close()
            socket.close()
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        DataSource.getDataSource().updateList(SocketParams.returnStringList())
    }

    private fun parseMessage(msg: String?) {
        if (msg != null) {
            val lines = msg.lines()
            lines.forEach {
                if (tagConnection in it)
                    SocketParams.jsonConnections = JSONObject(it.substringAfter(delimiter))
                if (tagTransaction in it)
                    SocketParams.jsonTransactions = JSONArray(it.substringAfter(delimiter))
            }
        } else Log.d(TAG, "empty socket welcomeMsg")
    }
}