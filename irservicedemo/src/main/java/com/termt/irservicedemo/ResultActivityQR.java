package com.termt.irservicedemo;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.AppBarConfiguration;


public class ResultActivityQR extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;

    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result_qr);
        overridePendingTransition(0, 0);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("LSFD", "Finish");
                finish();
            }
        }, 2000);
    }

}