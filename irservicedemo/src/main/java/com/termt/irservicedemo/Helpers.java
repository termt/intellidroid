package com.termt.irservicedemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Helpers {
    public static void sleepSafe(int ms){
        try {
            Thread.sleep( ms );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static String getProp(String prop)
    {
        Process process = null;
        BufferedReader bufferedReader = null;
        String GETPROP_EXECUTABLE_PATH = "/system/bin/getprop";

        try
        {
            process = new ProcessBuilder().command(GETPROP_EXECUTABLE_PATH, prop).redirectErrorStream(true).start();
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = bufferedReader.readLine();

            if (line == null)
            {
                line = ""; //prop not set
            }

            return line;
        }
        catch (Exception e)
        {
            return "";
        }
        finally
        {
            if (bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch (IOException e)
                {
                }
            }

            if (process != null)
            {
                process.destroy();
            }
        }
    }
}
