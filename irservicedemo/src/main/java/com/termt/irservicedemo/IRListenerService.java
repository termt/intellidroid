package com.termt.irservicedemo;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.termt.intellireader.entities.TransferResult;
import com.termt.intellireader.modules.ComplexPoll;
import com.termt.intellireader.modules.Miscellaneous;
import com.termt.intellireader.transfer.IRTransfer;
import com.termt.intellireader.transport.IRTransportSerial;
import com.termt.gpioservice.GPIOManager;
import java.util.ArrayList;

import complex.poll.PollForEventOuterClass;
import contactless.poll.PollForTokenOuterClass;
import contactless.token.TokenOuterClass;
import misc.buzzer.Buzzer;
import misc.leds.LedsOuterClass;
import qrcode.event.QrcodeEvent;
import qrcode.poll.PollForQrcode;

public class IRListnerService extends Service {

    private static Boolean run = false;

    private static GPIOManager.GPIOPin pin_open_turnstile;
    private static GPIOManager.GPIOPin pin_check_pass;

    //==============================================================================================
    // Open reader, using device-specific address and baudrate stored in android system properties
    // Creates a IRTransfer object for reader communication.
    // NOTE: IRTransfer object not thread-safe, be careful with multithreading
    //==============================================================================================
    public static IRTransfer openReader()
    {
        String addr = Helpers.getProp("persist.sys.irdev");
        String baud = Helpers.getProp("sys.irbaudrate");
        Log.e("Service", "Opening " + addr + " " + baud);

        try {
            IRTransportSerial serial_transport = new IRTransportSerial(addr, Integer.parseInt(baud), 1000); //timout ms
            return new IRTransfer(serial_transport);
        }
        catch (Exception e)
        {
            Log.e("IRSR","Reader busy");
        }
        return null;
    }

    //==============================================================================================
    // Check if reader transfer is ok, not error type
    //==============================================================================================
    public static boolean irsuccess(TransferResult r)
    {
        return !r.getClass().equals(TransferResult.Error.class);
    }

    //==============================================================================================
    // Ask reader to wait for a event, till it returns something meaningful
    // Event type is provided in _poll_ parameter
    //==============================================================================================
    public static TransferResult complexPollReader(IRTransfer t, PollForEventOuterClass.PollForEvent poll)
    {
        TransferResult waiting_for_token_result = null;
        while ((waiting_for_token_result == null || !irsuccess(waiting_for_token_result)))
        {
            Log.d("IRSRV", "please tap mifare card or scan qr");
            waiting_for_token_result = new ComplexPoll(t).pollForEvent(poll);
        }
        return waiting_for_token_result;
    }

    //==============================================================================================
    // Build an event description reader shoul poll for.
    // In this case - QR code enabled, Contactless token enabled.
    //==============================================================================================
    public static  PollForEventOuterClass.PollForEvent makeQrAndCtlssTokenPoll()
    {
        return PollForEventOuterClass.PollForEvent.newBuilder()
                .setPollForQrcode(PollForQrcode.PollForQrCode.newBuilder()
                        .setTimeout(1000)
                        .build())
                .setPollForToken(PollForTokenOuterClass.PollForToken.newBuilder()
                        .setTimeout(4000)
                        .setPollingMode(PollForTokenOuterClass.PollingMode.LOW_POWER_POLLING)
                        .setPreferMifare(false)
                        .build())
                .build();
    }


    //==============================================================================================
    // Build an OK sound sequence
    //==============================================================================================
    public static ArrayList<Buzzer.Note> makeOKNotes()
    {
        ArrayList<Buzzer.Note> notes = new ArrayList<>();
        notes.add(Buzzer.Note.newBuilder().setDurationMs(250).setFrequencyHz(1500)
                .setSilenceDurationMs(10).build());
        notes.add(Buzzer.Note.newBuilder().setDurationMs(250).setFrequencyHz(1500)
                .setSilenceDurationMs(10).build());
        return notes;
    }

    //==============================================================================================
    // Prepare device GPIO for gates
    //==============================================================================================
    public static void prepareGPIO()
    {
        pin_open_turnstile = GPIOManager.getInstance().getPin(GPIOManager.GPIOPins.PIN_OPTO_OUT);
        pin_check_pass = GPIOManager.getInstance().getPin(GPIOManager.GPIOPins.PIN_OPTO_IN);

        pin_open_turnstile.setDirection(GPIOManager.DIRECTION_OUT);
        pin_open_turnstile.setValue(GPIOManager.L_LOW);

        pin_check_pass.setDirection(GPIOManager.DIRECTION_IN);
    }

    //==============================================================================================
    // Use device GPIO to open gates
    //==============================================================================================
    public static void openGate()
    {
        pin_open_turnstile.setValue(GPIOManager.L_HIGH);

        waitForPass();

        pin_open_turnstile.setValue(GPIOManager.L_LOW);
    }

    //==============================================================================================
    // Use device GPIO to check passage sensor
    //==============================================================================================
    public static boolean waitForPass()
    {
        long tstart = System.currentTimeMillis();
        while (true)
        {
            if (pin_check_pass.getValue() == GPIOManager.L_HIGH)
            {
                Log.d("IRLS", "got pass check signal");
                return true;
            }

            long tend = System.currentTimeMillis();
            long tdelta = tend - tstart;
            double elapsedSeconds = tdelta / 1000.0;
            if (elapsedSeconds > 5){
                Log.d("IRLS", "pass timeout");
                return false;
            }
            Helpers.sleepSafe(50);
        }
    }

    //==============================================================================================
    // Process result for contactless token poll event
    //==============================================================================================
    public static void processContactlessTokenResult(Context ctx, IRTransfer t, TokenOuterClass.Token token)
    {
        LedsOuterClass.Leds CARD_EVENT = LedsOuterClass.Leds.newBuilder()
                .setBlue(true)
                .setRed(false)
                .setYellow(false)
                .setGreen(false)
                .build();

        new Miscellaneous(t).setLeds(CARD_EVENT);
        new Miscellaneous(t).makeSound(makeOKNotes());

        Log.e("IRLS", "Got token with type: " + token.getType());
        Intent dialogIntent = null;

        switch (token.getType()) {
            case UNKNOWN:
            case TAG_N_PLAY:
            case STM_SRI512:
            case ISO_14443_4A:
            case ISO_14443_4B:
            case SMART_MX_WITH_MIFARE_1K:
            case SMART_MX_WITH_MIFARE_4K:
                dialogIntent = new Intent(ctx, ResultActivityEMV.class);
                break;
            case MIFARE_CLASSIC_1K:
            case MIFARE_CLASSIC_2K:
            case MIFARE_CLASSIC_4K:
            case MIFARE_CLASSIC_MINI:
            case MIFARE_PLUS_X_SL2_2K:
            case MIFARE_PLUS_X_SL2_4K:
            case MIFARE_UL_OR_ULC:
                dialogIntent = new Intent(ctx, ResultActivityMifare.class);
                break;

        }
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        ctx.startActivity(dialogIntent);
    }

    //==============================================================================================
    // Process result for QR token poll event
    //==============================================================================================
    public static void processQrCodeResult(Context ctx, IRTransfer t, QrcodeEvent.QrCodeEvent tokenQr)
    {
        LedsOuterClass.Leds QR_EVENT = LedsOuterClass.Leds.newBuilder()
                .setBlue(false)
                .setRed(false)
                .setYellow(true)
                .setGreen(false)
                .build();
        new Miscellaneous(t).setLeds(QR_EVENT);
        new Miscellaneous(t).makeSound(makeOKNotes());

        Log.e("IRLS","Got QRevent with raw data: " + tokenQr.hasRawData() + "; serialized size: " + tokenQr.getSerializedSize());
        Intent dialogQrIntent = new Intent(ctx, ResultActivityQR.class);
        dialogQrIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        dialogQrIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        ctx.startActivity(dialogQrIntent);
    }

    //==============================================================================================
    @SuppressLint("MissingPermission")
    private static void GPSOn(Context ctx)
    {
        String provider = Settings.Secure
                .getString(ctx.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps"))
        { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            ctx.sendBroadcast(poke);
        }

        LocationManager location_manager = (LocationManager) ctx.getSystemService(LOCATION_SERVICE);

        location_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0,    new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                Log.d("IRSL", "onLocationChanged");
                String info = "Lat : " + location.getLatitude() + "; ";
                info += "Lon : " + location.getLongitude() + "; ";
                info += "Time : " + location.getTime() + "; ";
                info += "Acc : " + location.getAccuracy() + "; ";
                Log.d("IRSL", info);
            }
        });

        location_manager.registerGnssStatusCallback(new GnssStatus.Callback() {
            @Override
            public void onSatelliteStatusChanged(@NonNull GnssStatus status) {
                super.onSatelliteStatusChanged(status);
                Log.d("IRSL", "Satellites: " + status.getSatelliteCount());
            }
        });
    }

    //==============================================================================================
    private void GPSOff(Context ctx)
    {
        String provider = Settings.Secure
                .getString(ctx.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps"))
        { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            ctx.sendBroadcast(poke);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Toast.makeText(this, "IR Listner started", Toast.LENGTH_SHORT).show();
        NotificationChannel serviceChannel = new NotificationChannel("irlc1", "Example Service",NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(serviceChannel);

        Notification notification =
                new Notification.Builder(this, "irlc1")
                        .setContentTitle("IRListner service")
                        .setContentText("Running")
                        .build();

        this.run = true;

        Context ctx = this;

        //register gnss status and location updates
        GPSOn(ctx);


        prepareGPIO();

        //==============================================================================================
        // Main thread, it opens reader, poll for token, process results
        //==============================================================================================
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {

                        LedsOuterClass.Leds WAIT_LEDS = LedsOuterClass.Leds.newBuilder()
                                .setBlue(true)
                                .setRed(false)
                                .setYellow(false)
                                .setGreen(false)
                                .build();

                        LedsOuterClass.Leds DISABLE_ALL_LEDS = LedsOuterClass.Leds.newBuilder()
                                .setBlue(false)
                                .setRed(false)
                                .setYellow(false)
                                .setGreen(false)
                                .build();

                        while (run) {

                            IRTransfer t = openReader();

                            if (t == null){
                                Helpers.sleepSafe(1000);
                                continue;
                            }

                            new Miscellaneous(t).setLeds(WAIT_LEDS);

                            PollForEventOuterClass.PollForEvent complexPollEvent = makeQrAndCtlssTokenPoll();
                            TransferResult waiting_for_token_result = complexPollReader(t, complexPollEvent);

                            if (waiting_for_token_result == null)
                            {
                                Helpers.sleepSafe(1000);
                                continue;
                            }

                            PollForEventOuterClass.Event unwrapped = (PollForEventOuterClass.Event) waiting_for_token_result.unwrap();
                            if (unwrapped.hasContactlessToken()) {
                                processContactlessTokenResult(ctx, t, unwrapped.getContactlessToken());
                            } else if (unwrapped.hasQrcode()) {
                                processQrCodeResult(ctx, t, unwrapped.getQrcode());
                            }

                            new Miscellaneous(t).setLeds(DISABLE_ALL_LEDS);

                            openGate();
                        }
                    }
                }
        ).start();

        startForeground(1, notification);
        return START_STICKY;
    }

    //on destroy method will be called when the service is destroyed.
    @Override
    public void onDestroy() {
        super.onDestroy();
        GPSOff(this);
        this.run = false;
    }

    //below is the onBind method.
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
