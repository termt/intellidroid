package com.termt.irservicedemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startForegroundService(new Intent(MainActivity.this, IRListenerService.class));

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("LSFD", "Finish");
                finish();
            }
        }, 2000);
    }
}
